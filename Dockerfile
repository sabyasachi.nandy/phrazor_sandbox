FROM python:3.6-slim
RUN apt-get -y update
RUN apt-get install -y gcc libc-dev g++ libffi-dev libxml2 libffi-dev unixodbc-dev default-libmysqlclient-dev
COPY requirements.txt requirements.txt
COPY code_runner code_runner
RUN pip install numpy
RUN pip install -r requirements.txt
WORKDIR /sandbox