from .config import DEFAULT_USER


class Configuration:
	"""
	The configuration obj for the docker image to be created.
	"""

	def __init__(self, name, docker_image, command=None, user=DEFAULT_USER, read_only=False, network_disabled=True):
		self.name = name
		self.docker_image = docker_image
		self.command = command
		self.user = user
		self.read_only = read_only
		self.network_disabled = network_disabled


def driver(configs, docker_url=None):
	"""
	This function will later be replaced by an api.
	:return: None
	"""
	if not configs:
		raise ValueError("Atleast one config has to provided")
	if not docker_url:
		raise ValueError("Docker url is needed.")
	configs_map = {name: Configuration(name, **configs_kwargs) for name, configs_kwargs in configs.items()}
