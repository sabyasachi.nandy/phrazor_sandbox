import random
import docker
from docker import DockerClient
from requests.adapters import HTTPAdapter
from urllib3 import Retry
from . import config


class DockerClientManager:
	"""
	A singleton class, so that only one instance of clients pool object is present.
	"""
	_instance = None
	clients_pool = {}

	def __new__(cls):
		if DockerClientManager._instance is not None:
			return cls._instance
		else:
			instance = cls._instance = super(DockerClientManager, cls).__new__(cls)
			return instance

	def __str__(self):
		return ",".join(self.clients_pool.keys())

	@classmethod
	def get_docker_client(
			cls,
			base_url=config.BASE_URL,
			retry_read=config.DOCKER_MAX_READ_RETRIES,
			retry_status_force_list=(500,)):
		no_of_clients = len(cls.clients_pool.keys())
		if no_of_clients >= config.MAX_CLIENTS:
			client_key = f"Client_{random.randint(0,3)}"
			return cls.clients_pool[client_key]

		client_key = f"Client_{no_of_clients}"
		if client_key not in cls.clients_pool:
			"""
			We create a docker client manually and not from env variables directly
			This gives us more control, and does interfere with other clients.
			Docker Time Out specifies the timeout for docker api calls.
			"""
			client = docker.DockerClient(base_url=base_url, timeout=config.DOCKER_TIMEOUT)
			# Using retry object for creating the https docker client
			retries = Retry(
				total=config.DOCKER_MAX_TOTAL_RETRIES,
				connect=config.DOCKER_MAX_CONNECT_RETRIES,
				read=retry_read,
				method_whitelist=False,
				status_forcelist=retry_status_force_list,
				backoff_factor=config.DOCKER_BACKOFF_FACTOR,
				raise_on_status=False
			)
			http_adapter = HTTPAdapter(max_retries=retries)
			client.api.mount('https://', http_adapter)
			cls.clients_pool[client_key] = client
			return client
		else:
			return cls.clients_pool[client_key]

	def get_clients_list(self):
		return list(self.clients_pool.keys())

	def get_image_list(self, client_key=None):
		if not client_key:
			if not self.get_clients_list():
				raise ValueError("No client to connect, please create an connect first.")
			else:
				client_key = self.get_clients_list()[0]
		if client_key not in self.clients_pool:
			raise ValueError("Client not found, please check the client name")
		return self.clients_pool[client_key].images.list()

	def get_container_list(self, client_key=None, **kwargs):
		if not client_key:
			if not self.get_clients_list():
				raise ValueError("No client to connect, please create an connect first.")
			else:
				client_key = self.get_clients_list()[0]
		if client_key not in self.clients_pool:
			raise ValueError("Client not found, please check the client name")
		return self.clients_pool[client_key].containers.list(**kwargs)

	def close_client(self, docker_client):
		if not isinstance(docker_client, DockerClient):
			raise ValueError("Not a docker client")
		hex_add = hex(id(docker_client))
		client_key = None
		for key, value in self.clients_pool.items():
			if hex(id(value)) == hex_add:
				client_key = key
		if not client_key:
			raise ValueError("Client not created by the api")
		client = self.clients_pool[client_key]
		client.close()
		del self.clients_pool[client_key]



