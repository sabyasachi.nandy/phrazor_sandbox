DEFAULT_USER = 1000
BASE_URL = "unix://var/run/docker.sock"
DOCKER_MAX_TOTAL_RETRIES = 5
DOCKER_MAX_CONNECT_RETRIES = 5
DOCKER_MAX_READ_RETRIES = 5
DOCKER_BACKOFF_FACTOR = 0.2
DOCKER_TIMEOUT = 3000
MAX_CLIENTS = 4
WORKING_DIR = "/sandbox"
DEFAULT_COMMAND_LIST = ["/bin/sh"]
DEFAULT_LIMITS = {
	'cputime': 1,  # CPU time in seconds, None for unlimited
	'realtime': 5,  # Real time in seconds, None for unlimited
	'memory': 64,  # Memory in megabytes, None for unlimited
}
