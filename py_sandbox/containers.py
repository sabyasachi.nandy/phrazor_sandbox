import io
import tarfile
import time

__author__ = "Sabyasachi Nandy"
from docker.errors import DockerException, APIError
from requests import RequestException
from py_sandbox.utils import create_u_limits
from .config import *

"""
This files has all the function wrt to docker containers.
"""


class Containers:
    """
    Create, maintain and work on docker containers.
    * Note There should be a check on how this containers are created. Infinite
    creation should not be allowed
    """

    def __init__(self, container_id, image, command=None, limits=None, user=None, **kwargs):
        if command and not isinstance(command, list):
            raise ValueError("Command has to be of type list")
        if limits and not isinstance(limits, dict):
            raise ValueError("Command has to be of type list")

        self.container_id = container_id
        self.image = image
        self.command = command if command else DEFAULT_COMMAND_LIST
        self.limits = limits if limits else DEFAULT_LIMITS
        self.user = user if user else DEFAULT_USER
        self.container = None

    def create_container(self, docker_client):
        mem_limit = str(self.limits['memory']) + 'm'
        # TODO Volume logic
        u_limits = create_u_limits(self.limits)
        print("Creating Container")
        try:
            self.container = docker_client.containers.create(
                self.image,
                command=self.command,
                user=self.user,
                stdin_open=True,
                network_disabled=True,
                name=self.container_id,
                working_dir=WORKING_DIR,
                read_only=False,
                mem_limit=mem_limit,
                memswap_limit=mem_limit,  # Prevent from using any swap
                ulimits=u_limits,
                log_config={'type': 'none'}
            )
        except (RequestException, DockerException) as e:
            raise Exception(e)

    # if isinstance(e, APIError) and e.response.status_code == 409:
    # 	# Container with this name already exists
    # 	c = {'id': self.container_id}
    # 	self.container = docker_client.containers.get(self.container_id)
    # # TODO Raise error
    # pass

    def destroy_container(self):
        try:
            self.container.remove()
        except APIError as e:
            print("Could not remove container")
            pass

    def start_container(self):
        try:
            self.container.start()
        except APIError as e:
            print("Could not start container", str(e))
            self.destroy_container()

    def write_files_container(self, docker_client, files):
        """Write files to the working directory in the given container."""
        # Retry on 'No such container' since it may happen when the function
        # is called immediately after the container is created.
        # Retry on 500 Server Error when untar cannot allocate memory.
        m_time = int(time.time())
        files_written = []
        tarball_file_obj = io.BytesIO()
        with tarfile.open(fileobj=tarball_file_obj, mode='w') as tarball:
            for file in files:
                if not file.get('name') or not isinstance(file['name'], str):
                    continue
                content = file.get('content', b'')
                file_info = tarfile.TarInfo(name=file['name'])
                file_info.size = len(content)
                file_info.mtime = m_time
                tarball.addfile(file_info, fileobj=io.BytesIO(content))
                files_written.append(file['name'])
        try:
            docker_client.api.put_archive(
                self.container_id, WORKING_DIR,
                tarball_file_obj.getvalue()
            )
        except (RequestException, DockerException) as e:
            print("Exception")
