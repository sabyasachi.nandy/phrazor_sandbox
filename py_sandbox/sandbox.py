__author__ = "Sabyasachi Nandy"
from py_sandbox.clients import DockerClientManager
"""
Contains all utils necessary for creating, starting and  maintaining docker images.
"""


class PyDockerSandbox:
	"""
	The python docker sandbox main class.
	It will have function to create, start, destroy images.
	It will also handle running python scripts in the container and returning it.
	"""

	def __init__(self, docker_client, files):
		self.docker_client = docker_client
		self.files = files


def demo():
	# Test closing of the docker client.
	client_manager_obj = DockerClientManager()
	docker_client = client_manager_obj.get_docker_client()
	# Code for testing writing to working directory and executing code.

	print(client_manager_obj.get_container_list(docker_client, all=True))

