from docker.types import Ulimit


def create_u_limits(limits):
    u_limits = []
    if limits['cputime']:
        cpu = limits['cputime']
        u_limits.append(Ulimit(name='cpu', soft=cpu, hard=cpu))
    if 'file_size' in limits:
        f_size = limits['file_size']
        u_limits.append(Ulimit(name='fsize', soft=f_size, hard=f_size))
    return u_limits or None
