import pytest
from docker import DockerClient

from py_sandbox.clients import *


def test_singleton_client_pool_manager():
    client_manager_obj = DockerClientManager()
    address = hex(id(client_manager_obj))
    client_manager_obj_1 = DockerClientManager()
    address_1 = hex(id(client_manager_obj_1))
    print(address, address_1)
    assert address == address_1


def test_get_docker_client():
    client_manager_obj = DockerClientManager()
    docker_client = client_manager_obj.get_docker_client()
    assert isinstance(docker_client, DockerClient) == True


def test_max_get_docker_client():
    # Call docker client 100 times but ensure no more that 4 client objects are created.
    client_manager_obj = DockerClientManager()
    for i in range(100):
        docker_client = client_manager_obj.get_docker_client()
        max_client = len(client_manager_obj.get_clients_list())
        assert max_client < 5


def test_close_docker_client():
    # Test closing of the docker client.
    client_manager_obj = DockerClientManager()
    docker_client = client_manager_obj.get_docker_client()
    clients_list = client_manager_obj.get_clients_list()
    clients_list_old_len = len(clients_list)
    client_manager_obj.close_client(docker_client)
    clients_list = client_manager_obj.get_clients_list()
    assert len(clients_list) == (clients_list_old_len - 1)


def test_docker_image_list():
    client_manager_obj = DockerClientManager()
    clients_list = client_manager_obj.get_clients_list()
    assert isinstance(client_manager_obj.get_image_list(clients_list[0]), list) == True
