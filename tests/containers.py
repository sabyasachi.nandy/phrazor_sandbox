import random
import pytest

from py_sandbox.clients import DockerClientManager
from py_sandbox.containers import *


# def test_container_creation():
# 	client_manager_obj = DockerClientManager()
# 	docker_client = client_manager_obj.get_docker_client()
# 	image_list = client_manager_obj.get_image_list()
# 	python_image = None
# 	for image in image_list:
# 		print(image.tags)
# 		if "python:3.6-slim" in image.tags:
# 			python_image = image
# 	container_obj = Containers(container_id=f"PySandbox_{random.randint(0,10000)}", image=python_image)
# 	container_obj.create_container(docker_client)
#
#
# def test_container_destroy():
# 	client_manager_obj = DockerClientManager()
# 	docker_client = client_manager_obj.get_docker_client()
# 	image_list = client_manager_obj.get_image_list()
# 	python_image = None
# 	for image in image_list:
# 		print(image.tags)
# 		if "python:3.6-slim" in image.tags:
# 			python_image = image
# 	container_obj = Containers(container_id=f"PySandbox_{random.randint(0,10000)}", image=python_image)
# 	container_obj.create_container(docker_client)
# 	container_obj.destroy_container()


def test_write_to_container():
	client_manager_obj = DockerClientManager()
	docker_client = client_manager_obj.get_docker_client()
	image_list = client_manager_obj.get_image_list()
	python_image = None
	for image in image_list:
		print(image.tags)
		if "code_runner:latest" in image.tags:
			python_image = image
	print("Python Image", python_image)
	container_obj = Containers(container_id=f"PySandbox_{random.randint(0,10000)}", image=python_image)
	container_obj.create_container(docker_client)
	container_obj.write_files_container(docker_client, [{'name': 'main.py', 'content': b'print(42)'}])
	container_obj.start_container()