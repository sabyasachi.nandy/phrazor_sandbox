import os

from py_sandbox import utils
from code_runner.code_runner import CodeRunner

if __name__ == "__main__":
    code_path = os.path.join(os.getcwd(), 'code_runner', 'code_test_dir', 'correct_code.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    code_runner_obj.execute_code_with_restrictions()
