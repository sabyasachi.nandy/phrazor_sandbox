import os


def check_result(result):
    """
    This function checks the type of result returned after executing the code.
    This also calculates added meta info like total memory usage
    We expect every code to return data in a dict format.
    :param result: The result of code execution
    :return: dict, containing info about return type, a message and the status.
    """
    to_return = {}
    if not result:
        to_return.update({
            'return_type': "None",
            'message': ["Code returned none, please send a dict"],
            'status': 'error'
        })
    elif not isinstance(result, dict):
        to_return.update({
            'return_type': str(type(result)),
            'message': ["Return type not accepted, please send a dict"],
            'status': 'error'
        })
    else:
        to_return.update({
            'return_type': "dict",
            'message': ["Return type is correct"],
            'status': 'success'
        })
    with open('/proc/{}/status'.format(os.getpid())) as f:
        mem_usage = int(f.read().split('VmRSS:')[1].split('\n')[0][:-3].strip())
        to_return.update({
            'memory_usage': mem_usage
        })
        print('Returned', result, to_return)
        return to_return
