import os

import pytest

from code_runner.code_runner import CodeRunner
from code_runner.exceptions import CodeRunnerPackageException


def test_code_runner_init_and_validity():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'report28.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    assert code_runner_obj.is_valid() == True


def test_code_runner_init_and_validity_fail():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'report29.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    assert code_runner_obj.is_valid() == False
    assert len(code_runner_obj.errors) > 0


def test_get_main_function():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'report28.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    with pytest.raises(CodeRunnerPackageException):
        if code_runner_obj.is_valid():
            main_function = code_runner_obj.get_function()
    print(code_runner_obj.errors)


def test_style_checker_error():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'report.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    code_runner_obj.style_checker()
    assert code_runner_obj.style_check_status == "error"


def test_style_checker_error_success():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'correct_code.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    code_runner_obj.style_checker()
    print(code_runner_obj.style_check_output)
    assert code_runner_obj.style_check_status == "success"


def test_code_runner_check():
    root_dir = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    code_path = os.path.join(root_dir, 'code_test_dir', 'correct_code.py')
    code_runner_obj = CodeRunner(docker_id="123", code_path=code_path, **{'test': '123'})
    code_runner_obj.execute_code_with_restrictions()
    import time
    time.sleep(20)
