class CodeRunnerExceptions(Exception):
    def __init__(self, message, info=None):
        self.message = message
        self.info = info

    def __str__(self):
        return self.message


class CodeRunnerPackageException(CodeRunnerExceptions):
    pass


class CodeRunnerStyleCheckException(CodeRunnerExceptions):
    pass


class CodeExecuteException(CodeRunnerExceptions):
    pass
