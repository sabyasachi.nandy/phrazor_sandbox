"""
This module is responsible for running a python code received from the client.
It receives a code and necessary arguments for running the code.
It has information to connect to suitable database to get the data needed for running the code.
This will be run inside a docker container.
"""
import datetime
import importlib
import os
import resource
import signal
import subprocess
import time
import types
import magic
from .exceptions import *
from .utils import check_result

__author__ = "Sabyasachi Nandy"
__email__ = "sabyasachi.nandy@vphrase.com"

BASE_FUNCTION_NAME = "main"


class CodeRunner:
    """
    Main class for running a code submitted by the client.
    It will read the code from a docker volume/directory with help of arguments submitted it.
    It will then connect to a database to fetch the necessary data for running that code.
    It will execute the code, under certain condition and written additional info like run time, mem usage
    with the actual result. In case of any exception, the exception will be handled and proper error message to be
    send.
    """

    def __init__(self, docker_id, code_path, *args, **kwargs):
        """
        :param docker_id: The docker container which is running the code.
        :param code_path: The path to the code_dir
        :param code_args: The additional args for the code.
        """
        self.docker_id = docker_id
        self.code_path = code_path
        self.code_args = kwargs
        self.errors = []
        self.style_check_output = []
        self.style_check_status = None

    def is_valid(self):
        """
        Get the data for the code, from the code path get the actual code.
        Do some validation over the code, before running it.
        :return: True if valid, else raise Validation error
        """
        if not isinstance(self.code_args, dict):
            self.errors.append("Please pass the extra arguments as keyword arguments")
        if not os.path.isfile(self.code_path):
            self.errors.append("Code file does not exists in the mentioned path.")
        if os.path.isfile(self.code_path) and 'Python script' not in magic.from_file(self.code_path):
            self.errors.append("Code file passed is not a python script")
        if self.errors:
            print('errors', self.errors)
            return False
        return True

    def get_function(self):
        """
        This function returns the main function from the code by dynamically importing it.
        **note:
            * This function is dependent on the global Base function name.
        :return: A function object.
        """
        file_path, file_name = os.path.split(self.code_path)
        loader = importlib.machinery.SourceFileLoader(file_name, self.code_path)
        mod = types.ModuleType(loader.name)
        try:
            loader.exec_module(mod)
            return getattr(mod, BASE_FUNCTION_NAME)
        except ModuleNotFoundError as e:
            message = "A module is used which is not yet supported. Please contact admin."
            self.errors.append(message)
            raise CodeRunnerPackageException(
                message=message,
                info={
                    'actual_message': str(e),
                    'traceback': e.__traceback__
                })
        except Exception as e:
            message = "Encountered an unexpected exception while importing the main function of the code."
            self.errors.append(message)
            raise CodeRunnerPackageException(
                message=message,
                info={
                    'actual_message': str(e),
                    'traceback': e.__traceback__
                })

    def style_checker(self):
        """
        This function does style check for the code submitted to be executed.
        """
        config = ['flake8', self.code_path]
        try:
            result = subprocess.Popen(
                config,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT
            )
        except FileNotFoundError as e:
            message = "File needed for style check not found"
            if 'flake8' in str(e):
                message = "Flake 8 may be missing, check if its installed or not"
            self.errors.append(message)
            raise CodeRunnerStyleCheckException(
                message=message,
                info={
                    'actual_message': str(e),
                    'traceback': e.__traceback__
                }
            )
        _list = result.communicate()[0].splitlines()
        _list = [x.decode() for x in _list]
        _list = [x.split(':', 1)[1] for x in _list]
        _list = [x.split(' ', 1) for x in _list]
        # example output of error ['58:76:', 'E128 continuation line under-indented for visual indent']
        status = 'success'
        for elements in _list:
            if elements[1][0] == 'E':
                status = 'error'
                break
            elif elements[1][0] == 'W':
                status = 'warning'
        self.style_check_output = _list
        self.style_check_status = status

    def execute_code(self):
        try:
            main_function = self.get_function()
        except CodeRunnerPackageException as e:
            raise CodeExecuteException(message=e.message, info=e.info)
        start = datetime.datetime.now()
        # TODO Exception Handling Here
        try:
            result = main_function()
        except Exception as e:
            message = "Exception in executing code"
            self.errors.append(message)
            raise CodeExecuteException(
                message=message,
                info={
                    'actual_message': str(e),
                    'traceback': e.__traceback__
                }
            )
        time_taken = datetime.datetime.now() - start
        package_result = check_result(result)
        package_result.update({
            'run_time': time_taken.seconds
        })
        return package_result

    def signal_handler(self, signum, frame):
        # Handle this properly.
        print("Caught")

    def execute_code_with_restrictions(self):
        print("Test")
        resource.setrlimit(resource.RLIMIT_CPU, (2, resource.RLIM_INFINITY))
        signal.signal(signal.SIGXCPU, self.signal_handler)  # initiate the handler of signal of Time limit exceeded.
        self.execute_code()
