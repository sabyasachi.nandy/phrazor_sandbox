def arrow(value, type='incr', color='green'):
	if type == 'incr' or type == 'increasing':
		arrow = "<i class='fa fa-arrow-up' aria-hidden='true' style='color:" + color + ";'></i>"
	else:
		arrow = "<i class='fa fa-arrow-down' aria-hidden='true' style='color:" + color + ";'></i>"
	return str(str(value) + '&nbsp;' + arrow)


def main(files=[], filters=[]):
	global df, table_data
	if len(files) != 0:
		df = files
	df_list = {}
	for files, value in df.items():
		sheets = value
		for sheet, value in sheets.items():
			df_list.update({
				sheet: value
			})
	df = df_list
	# print(df.keys())
	# print(df['DPO1711'])
	# print(df['DPO1809'])
	###### FILTERS AND FILE READ...................................................
	time = 1809
	time_period = 'YTD'
	currency = 'INR'

	# get filter values
	if len(filters) > 0:
		first_filter = filters[0]['selected']
		if first_filter != '' and first_filter != None:
			time = first_filter['value']
		else:
			time = 1809

		second_filter = filters[1]['selected']
		if second_filter != '' and second_filter != None:
			time_period = second_filter['value']
		else:
			time_period = 'YTD'

		third_filter = filters[2]['selected']
		if third_filter != '' and third_filter != None:
			currency = third_filter['value']
		else:
			currency = 'INR'

	start = timeit.default_timer()
	rate_df = df['CurrencyFilter']
	rate = rate_df[rate_df['Currency'] == currency]['conversion_rate'].values[0]
	currency = 'M' + currency
	time = str(time)
	month_code = str(time[2:])
	year_code = time[:2]
	last_year_code = str(int(year_code) - 1)
	year_code = str(year_code)
	month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
				  'November', 'December']
	month_codes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
	month = month_name[month_codes.index(month_code)]
	# year = int('20'+time[:2])
	m = (month_codes.index(month_code))
	# default qtd_month_code= 1 when not in use
	qtd_month_code = 1

	#     if (month_code!= '03' or month_code!= '06' or month_code!= '09') and time_period== 'QTD':
	#         #read individual sheets
	#         time_period= 'YTD'

	if time_period == 'YTD':
		# print(df.keys())
		# read individual sheets
		DIH_CM_CY = df['DIH' + year_code + month_code]
		DIH_CM_LY = df['DIH' + last_year_code + month_code]
		DSO_CM_CY = df['DSO' + year_code + month_code]
		DSO_CM_LY = df['DSO' + last_year_code + month_code]
		DPO_CM_CY = df['DPO' + year_code + month_code]
		DPO_CM_LY = df['DPO' + last_year_code + month_code]
		DIV_BU_MAP = df['LPGMapping']
		OVERDUE_CM_CY = df['Overdues' + year_code + month_code]
		OVERDUE_CM_LY = df['Overdues' + last_year_code + month_code]
		NWC_COMP_CM_CY = df['NWC' + year_code + month_code]
		NWC_COMP_CM_LY = df['NWC' + last_year_code + month_code]
		RETENTION_CM_CY = df['Retention' + year_code + month_code]
		RETENTION_CM_LY = df['Retention' + last_year_code + month_code]
		RU_CM_CY = df['PerformanceRU' + year_code + month_code]
		DV_CM_CY = df['PerformanceDV' + year_code + month_code]

		RU_CM_CY_QTD = 1
		DV_CM_CY_QTD = 1

	else:
		#         if m > 8:
		#             m = m-12
		#         qtd_month_code = month_codes[m + 3]
		qtd_month_code = str(12)
		# print(df.keys())
		# read individual sheets
		# print(year_code)
		# print(month_code)
		# print(last_year_code)
		# print(qtd_month_code)
		DIH_CM_CY = df['DIH' + year_code + month_code]
		DIH_CM_LY = df['DIH' + last_year_code + qtd_month_code]
		DSO_CM_CY = df['DSO' + year_code + month_code]
		DSO_CM_LY = df['DSO' + last_year_code + qtd_month_code]
		DPO_CM_CY = df['DPO' + year_code + month_code]
		DPO_CM_LY = df['DPO' + last_year_code + qtd_month_code]
		DIV_BU_MAP = df['LPGMapping']
		OVERDUE_CM_CY = df['Overdues' + year_code + month_code]
		OVERDUE_CM_LY = df['Overdues' + last_year_code + qtd_month_code]
		NWC_COMP_CM_CY = df['NWC' + year_code + month_code]
		NWC_COMP_CM_LY = df['NWC' + last_year_code + qtd_month_code]
		RETENTION_CM_CY = df['Retention' + year_code + month_code]
		RETENTION_CM_LY = df['Retention' + last_year_code + qtd_month_code]
		RU_CM_CY = df['PerformanceRU' + year_code + month_code]
		DV_CM_CY = df['PerformanceDV' + year_code + month_code]

		RU_CM_CY_QTD = df['PerformanceRU' + last_year_code + qtd_month_code]
		DV_CM_CY_QTD = df['PerformanceDV' + last_year_code + qtd_month_code]
	# ..............................................................................
	# print(DPO_CM_CY)
	# print(df['DPO1811'])
	# getting the lists of divisions, BUs and PGs for mapping
	DIV_BU_MAP['LPG'] = DIV_BU_MAP['LPG'].astype(str)
	div_codes = DIV_BU_MAP['Div'].unique()
	div_codes = np.append(div_codes, 'ZC')
	bu_codes = DIV_BU_MAP['LBU'].unique()
	pg_codes = DIV_BU_MAP['LPG'].unique()

	zc_bu = list(DIV_BU_MAP[(DIV_BU_MAP['Div'] == 'ZC') & (DIV_BU_MAP['LBU'] != 'ZCPG')]['LBU'].unique())
	zc_pg = list(DIV_BU_MAP[(DIV_BU_MAP['Div'] == 'ZC') & (DIV_BU_MAP['LBU'] != 'ZCPG')]['LPG'].unique())
	zc_list = zc_bu + zc_pg
	print(zc_list)
	#     DIH_CM_CY= filter_zcpg(DIH_CM_CY, DIV_BU_MAP, zc_list)
	#     DIH_CM_LY= filter_zcpg(DIH_CM_LY, DIV_BU_MAP, zc_list)
	#     DSO_CM_CY= filter_zcpg(DSO_CM_CY, DIV_BU_MAP, zc_list)
	#     DSO_CM_LY= filter_zcpg(DSO_CM_LY, DIV_BU_MAP, zc_list)
	#     DPO_CM_CY= filter_zcpg(DPO_CM_CY, DIV_BU_MAP, zc_list)
	#     DPO_CM_LY= filter_zcpg(DPO_CM_LY, DIV_BU_MAP, zc_list)
	#     OVERDUE_CM_CY= filter_zcpg(OVERDUE_CM_CY, DIV_BU_MAP, zc_list)
	#     OVERDUE_CM_LY= filter_zcpg(OVERDUE_CM_LY, DIV_BU_MAP, zc_list)
	#     NWC_COMP_CM_CY= NWC_COMP_CM_CY[~NWC_COMP_CM_CY['BU'].isin(zc_bu)]
	#     NWC_COMP_CM_LY= NWC_COMP_CM_LY[~NWC_COMP_CM_LY['BU'].isin(zc_bu)]
	#     RETENTION_CM_CY= RETENTION_CM_CY[~RETENTION_CM_CY['BU'].isin(zc_bu)]
	#     RETENTION_CM_LY= RETENTION_CM_LY[~RETENTION_CM_LY['BU'].isin(zc_bu)]

	ccc_files = [DIH_CM_CY, DIH_CM_LY, DSO_CM_CY, DSO_CM_LY, DPO_CM_CY, DPO_CM_LY, OVERDUE_CM_CY, OVERDUE_CM_LY]
	for i in ccc_files:
		i = filter_zcpg(i, DIV_BU_MAP, zc_list)

	DIH_CM_CY = ccc_files[0]
	DIH_CM_LY = ccc_files[1]
	DSO_CM_CY = ccc_files[2]
	DSO_CM_LY = ccc_files[3]
	DPO_CM_CY = ccc_files[4]
	DPO_CM_LY = ccc_files[5]
	OVERDUE_CM_CY = ccc_files[6]
	OVERDUE_CM_LY = ccc_files[7]

	#     DIH_CM_CY= filter_zcpg(DIH_CM_CY, DIV_BU_MAP, zc_list)
	#     DIH_CM_LY= filter_zcpg(DIH_CM_LY, DIV_BU_MAP, zc_list)
	#     DSO_CM_CY= filter_zcpg(DSO_CM_CY, DIV_BU_MAP, zc_list)
	#     DSO_CM_LY= filter_zcpg(DSO_CM_LY, DIV_BU_MAP, zc_list)
	#     DPO_CM_CY= filter_zcpg(DPO_CM_CY, DIV_BU_MAP, zc_list)
	#     DPO_CM_LY= filter_zcpg(DPO_CM_LY, DIV_BU_MAP, zc_list)
	#     OVERDUE_CM_CY= filter_zcpg(OVERDUE_CM_CY, DIV_BU_MAP, zc_list)
	#     OVERDUE_CM_LY= filter_zcpg(OVERDUE_CM_LY, DIV_BU_MAP, zc_list)
	#     NWC_COMP_CM_CY= NWC_COMP_CM_CY.query('BU not in @zc_bu')
	#     NWC_COMP_CM_LY= NWC_COMP_CM_LY.query('BU not in @zc_bu')
	NWC_COMP_CM_CY = NWC_COMP_CM_CY[~NWC_COMP_CM_CY['BU'].isin(zc_bu)]
	NWC_COMP_CM_LY = NWC_COMP_CM_LY[~NWC_COMP_CM_LY['BU'].isin(zc_bu)]
	RETENTION_CM_CY = RETENTION_CM_CY[~RETENTION_CM_CY['BU'].isin(zc_bu)]
	RETENTION_CM_LY = RETENTION_CM_LY[~RETENTION_CM_LY['BU'].isin(zc_bu)]
	#     RETENTION_CM_CY= RETENTION_CM_CY.query('BU not in @zc_bu')
	#     RETENTION_CM_LY= RETENTION_CM_LY.query('BU not in @zc_bu')

	# here the files are being cleaned
	# DIH_CM_CY = clean_cashconversion_sheets(DIH_CM_CY, div_codes, bu_codes, pg_codes)
	# DIH_CM_LY = clean_cashconversion_sheets(DIH_CM_LY, div_codes, bu_codes, pg_codes)
	# DSO_CM_CY = clean_cashconversion_sheets(DSO_CM_CY, div_codes, bu_codes, pg_codes)
	# DSO_CM_LY = clean_cashconversion_sheets(DSO_CM_LY, div_codes, bu_codes, pg_codes)
	# DPO_CM_CY = clean_cashconversion_sheets(DPO_CM_CY, div_codes, bu_codes, pg_codes)
	# DPO_CM_LY = clean_cashconversion_sheets(DPO_CM_LY, div_codes, bu_codes, pg_codes)
	# OVERDUE_CM_CY= clean_overdue_sheet(OVERDUE_CM_CY, div_codes, bu_codes, pg_codes)
	# OVERDUE_CM_LY= clean_overdue_sheet(OVERDUE_CM_LY, div_codes, bu_codes, pg_codes)

	################ FUNCTION CALLING.........................................................

	sei_inv = comment_3_4(NWC_COMP_CM_CY, NWC_COMP_CM_LY, div_codes, bu_codes, pg_codes, currency, rate)
	sei_dict_list = sei_inv[0]
	sei = sort_list(sei_dict_list)
	sei_names = sei[1]
	sei_values = sei[0]

	inv_dict_list = sei_inv[1]
	inv = sort_list(inv_dict_list)
	inv_names = inv[1]
	inv_values = inv[0]

	overdue = comment_2(OVERDUE_CM_CY, OVERDUE_CM_LY, div_codes, bu_codes, pg_codes, currency, rate)
	ovd_dict_list = overdue[0]
	ovd = sort_list(ovd_dict_list)
	ovd_names = ovd[1]
	ovd_values = ovd[0]

	ovd_inc, ovd_dec, len_ovd_inc, len_ovd_dec = bu_inc_dec(ovd_names, ovd_values, currency)
	sei_inc, sei_dec, len_sei_inc, len_sei_dec = bu_inc_dec(sei_names, sei_values, currency)
	inv_inc, inv_dec, len_inv_inc, len_inv_dec = bu_inc_dec(inv_names, inv_values, currency)

	ovd_sei_inv_loop = combine_list_obj(ovd_inc, ovd_dec, sei_inc, sei_dec, inv_inc, inv_dec, overdue[1], sei_inv[2],
										sei_inv[3], currency, len_ovd_inc, len_ovd_dec, len_sei_inc, len_sei_dec,
										len_inv_inc, len_inv_dec)

	ccc = comment_1(div_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY, bu_codes, pg_codes,
					year_code, last_year_code, month_code, qtd_month_code, time_period)
	ccc_graph = ccc[1]
	ccc_narrative = ccc[2]

	retention, retention_table = retention_graph(NWC_COMP_CM_CY, NWC_COMP_CM_LY, RETENTION_CM_CY, RETENTION_CM_LY,
												 currency, rate)

	overall_nwc, current_nwc = overall_nwc_func(RU_CM_CY, NWC_COMP_CM_CY, NWC_COMP_CM_LY, time_period, month_code,
												qtd_month_code, year_code, last_year_code, currency, RU_CM_CY_QTD, rate)

	table_nwc, nwc_graph = nwc_table(NWC_COMP_CM_CY, currency, month_code, year_code, qtd_month_code, last_year_code,
									 NWC_COMP_CM_LY, time_period, RU_CM_CY, DV_CM_CY, RU_CM_CY_QTD, DV_CM_CY_QTD, rate)

	table_ccc = ccc_table(DSO_CM_CY, DSO_CM_LY, DIH_CM_CY, DIH_CM_LY, DPO_CM_CY, DPO_CM_LY, time_period, year_code,
						  last_year_code, month_code, qtd_month_code, DV_CM_CY)

	result = {'retention': retention, 'overall_ccc_waterfall': ccc[3],
			  'retention table': retention_table,
			  # 'ccc_ep': ccc_graph[0], 'ccc_ia': ccc_graph[1], 'ccc_pg': ccc_graph[2], 'ccc_rm': ccc_graph[3], 'ccc_zc': ccc_graph[4],
			  'ccc_narrative': ccc_narrative,
			  'overall ccc': ccc[4], 'current ccc': ccc[5],
			  'overall_nwc': overall_nwc, 'current nwc': current_nwc,
			  'overall_nwc_val': commas(overall_nwc), 'current nwc_val': commas(current_nwc),
			  'nwc table': table_nwc,
			  'ccc table': table_ccc,
			  'nwc_graph': nwc_graph, 'ovd_sei_inv_loop': ovd_sei_inv_loop,

			  'ovd_name_1': ovd_names[0], 'ovd_val_1': ovd_values[0],
			  'ovd_name_2': ovd_names[1], 'ovd_val_2': ovd_values[1],
			  'ovd_name_3': ovd_names[2], 'ovd_val_3': ovd_values[2],
			  'ovd_name_4': ovd_names[3], 'ovd_val_4': ovd_values[3],
			  'ovd_name_5': ovd_names[4], 'ovd_val_5': ovd_values[4],
			  'overall overdue': overdue[1],

			  'sei_name_1': sei_names[0], 'sei_val_1': sei_values[0],
			  'sei_name_2': sei_names[1], 'sei_val_2': sei_values[1],
			  'sei_name_3': sei_names[2], 'sei_val_3': sei_values[2],
			  'sei_name_4': sei_names[3], 'sei_val_4': sei_values[3],
			  'sei_name_5': sei_names[4], 'sei_val_5': sei_values[4],
			  'overall sei': sei_inv[2],

			  'inv_name_1': inv_names[0], 'inv_val_1': inv_values[0],
			  'inv_name_2': inv_names[1], 'inv_val_2': inv_values[1],
			  'inv_name_3': inv_names[2], 'inv_val_3': inv_values[2],
			  'inv_name_4': inv_names[3], 'inv_val_4': inv_values[3],
			  'inv_name_5': inv_names[4], 'inv_val_5': inv_values[4],
			  'overall inv': sei_inv[3],

			  'time period': time_period, 'year code': year_code, 'last year code': last_year_code,
			  'month code': month_code, 'currency': currency,
			  'qtd month code': qtd_month_code, 'month': month
			  }

	print('time for entire:')
	print(timeit.default_timer() - start)
	return result
	return {'report_id': 1}


def filter_zcpg(file, DIV_BU_MAP, zc_list):
	#     code_list= list(file['sas_code'].apply(lambda x: x[4:]))
	#     file['filter_code']= code_list
	# sas_code_list= file['sas_code'].tolist()

	file['filter_code'] = file['sas_code'].apply(lambda x: x[4:])

	#     zc_bu= DIV_BU_MAP[(DIV_BU_MAP['Div']== 'ZC') & (DIV_BU_MAP['LBU']!= 'ZCPG')]['LBU'].unique()
	#     zc_pg= DIV_BU_MAP[(DIV_BU_MAP['Div']== 'ZC') & (DIV_BU_MAP['LBU']!= 'ZCPG')]['LPG'].unique()
	#     zc_list= zc_bu + zc_pg

	# file= file[~file['filter_code'].isin(zc_list)]

	file = file[~file['filter_code'].isin(zc_list)]
	# file= file.query('filter_code not in @zc_list')

	return file


def sort_list(list_of_list):
	names = list_of_list[0]
	values = list_of_list[1]
	#     for i in range(0, len(list_of_dict)):
	#         names.append(list_of_dict[i]['name'])
	#         values.append(list_of_dict[i][parameter])
	#     values, names  =   (      list(i) for i in zip( *sorted(zip(values, names), reverse= True) )      )
	values, names = (list(i) for i in zip(*sorted(zip(values, names), reverse=True)))
	return [values, names]


def commas(number):
	string = str(number)  # converting number to string

	flag = 'positive'
	if string[0] == '-':
		string = string[1:]
		flag = 'negative'

	# check if decimal part present
	if '.' in string:
		integer, decimal = string.split('.')  # split into decimal part and integer part
	else:
		integer = string

	length = len(integer)  # length of integer part

	# inserting commas after every three digits
	if length > 3:
		integer = integer[:-3] + "," + integer[-3:]
		if length > 6:
			integer = integer[:-7] + "," + integer[-7:]
			if length > 9:
				integer = integer[:-11] + "," + integer[-11:]
				if length > 12:
					integer = integer[:-15] + "," + integer[-15:]

	if '.' in string:
		final_val = integer + '.' + decimal
	else:
		final_val = integer

	if flag == 'negative':
		return '-' + final_val
	else:
		return final_val


def bu_inc_dec(names, change_list, currency):
	# start = timeit.default_timer()

	list_increase = []
	list_decrease = []

	bu_list = names
	for i, j in zip(change_list, bu_list):
		if i > 0:
			red_1 = "<span style='color:red'>" + str('+' + str(commas(i)) + ' ' + str(currency)) + "</span>"
			# red_2= "<span style='color:red'>"+str(j)+"</span
			red_2 = str(j)
			list_increase.append((red_1 + ' for ' + red_2))

		elif i < 0:
			list_decrease.append((str(commas(i)) + ' ' + str(currency) + ' for ' + str(j)))

		else:
			pass

	length_inc = len(list_increase)
	length_dec = len(list_decrease)

	list_obj_inc = {}
	list_obj_inc['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	list_obj_inc['value'] = list_increase

	list_obj_dec = {}
	list_obj_dec['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	list_obj_dec['value'] = list_decrease

	return list_obj_inc, list_obj_dec, length_inc, length_dec


def combine_list_obj(ovd_inc_obj, ovd_dec_obj, sei_inc_obj, sei_dec_obj, inve_inc_obj, inve_dec_obj,
					 val_overall_overdue, val_overall_sei, val_overall_inv, currency, ovd_inc_len, ovd_dec_len,
					 sei_inc_len, sei_dec_len, inve_inc_len, inve_dec_len):
	if val_overall_overdue > 0:
		red_1 = "<span style='color:red'>" + str(
			'+' + str(commas(val_overall_overdue)) + ' ' + str(currency)) + "</span>"
	else:
		red_1 = commas(val_overall_overdue) + ' ' + str(currency)

	if val_overall_sei > 0:
		red_2 = "<span style='color:red'>" + str('+' + str(commas(val_overall_sei)) + ' ' + str(currency)) + "</span>"
	else:
		red_2 = commas(val_overall_sei) + ' ' + str(currency)

	if val_overall_inv > 0:
		red_3 = "<span style='color:red'>" + str('+' + str(commas(val_overall_inv)) + ' ' + str(currency)) + "</span>"
	else:
		red_3 = commas(val_overall_inv) + ' ' + str(currency)
	# Must be returned with the same key name as the loop_obj['name']
	loop_obj = {}
	loop_obj['name'] = "loop_object"
	loop_obj['type'] = "dict"
	loop_obj['value'] = [
		{'sequence': 2, 'parameter_comp': val_overall_overdue, 'parameter': 'Third Party Overdues', 'overall': red_1,
		 'inc_list': ovd_inc_obj, 'dec_list': ovd_dec_obj, 'len_inc': ovd_inc_len, 'dec_len': ovd_dec_len},
		{'sequence': 3, 'parameter_comp': val_overall_sei, 'parameter': 'Contractual Assets', 'overall': red_2,
		 'inc_list': sei_inc_obj, 'dec_list': sei_dec_obj, 'len_inc': sei_inc_len, 'dec_len': sei_dec_len},
		{'sequence': 4, 'parameter_comp': val_overall_inv, 'parameter': 'Inventories', 'overall': red_3,
		 'inc_list': inve_inc_obj, 'dec_list': inve_dec_obj, 'len_inc': inve_inc_len, 'dec_len': inve_dec_len}
	]

	return loop_obj


def ccc_table(DSO_CM_CY, DSO_CM_LY, DIH_CM_CY, DIH_CM_LY, DPO_CM_CY, DPO_CM_LY, time_period, year_code, last_year_code,
			  month_code, qtd_month_code, DV_CM_CY):
	div_codes = ['EP', 'RM', 'IA', 'PG', 'ZC']

	try:
		cm_cy_overall_dso = round(DSO_CM_CY[DSO_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dso = 0

	try:
		cm_ly_overall_dso = round(DSO_CM_LY[DSO_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dso = 0

	try:
		cm_cy_overall_dih = round(DIH_CM_CY[DIH_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dih = 0

	try:
		cm_ly_overall_dih = round(DIH_CM_LY[DIH_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dih = 0

	try:
		cm_cy_overall_dpo = round(DPO_CM_CY[DPO_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dpo = 0

	try:
		cm_ly_overall_dpo = round(DPO_CM_LY[DPO_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dpo = 0

	cm_cy_ccc = round((cm_cy_overall_dso + cm_cy_overall_dih - cm_cy_overall_dpo))
	cm_ly_ccc = round((cm_ly_overall_dso + cm_ly_overall_dih - cm_ly_overall_dpo))

	#     value_impact_1= ( ((DV_CM_CY[DV_CM_CY['Line Item']=='Revenues']['FTM CY'].sum()/ 1000) + (DV_CM_CY[DV_CM_CY['Line Item']=='Revenues']['FTM LY'].sum()/1000)) / 2 )

	#     value_impact_2= (value_impact_1* 12) / 365

	#     value_impact= int(round(value_impact_2* (cm_cy_ccc- cm_ly_ccc)))

	CM_CY_CCC = div_ccc_comment(div_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY)
	CM_LY_CCC = div_ccc_comment(div_codes, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY)
	########################################## CCC TABLE
	dso_cy = []
	dso_ly = []
	dih_cy = []
	dih_ly = []
	dpo_cy = []
	dpo_ly = []
	ccc_cy = []
	ccc_ly = []
	val_impact = []

	dso_cy.append(commas(int(round(cm_cy_overall_dso))))
	dso_ly.append(commas(int(round(cm_ly_overall_dso))))
	dih_cy.append(commas(int(round(cm_cy_overall_dih))))
	dih_ly.append(commas(int(round(cm_ly_overall_dih))))
	dpo_cy.append(commas(int(round(cm_cy_overall_dpo))))
	dpo_ly.append(commas(int(round(cm_ly_overall_dpo))))
	ccc_cy.append(commas(int(cm_cy_ccc)))
	ccc_ly.append(commas(int(cm_ly_ccc)))
	#     val_impact.append(commas(value_impact))
	for i in div_codes:
		dso_cy.append(commas(int(round(CM_CY_CCC[i][1]))))
		dso_ly.append(commas(int(round(CM_LY_CCC[i][1]))))

		dih_cy.append(commas(int(round(CM_CY_CCC[i][2]))))
		dih_ly.append(commas(int(round(CM_LY_CCC[i][2]))))

		dpo_cy.append(commas(int(round(CM_CY_CCC[i][3]))))
		dpo_ly.append(commas(int(round(CM_LY_CCC[i][3]))))

		ccc_cy.append(commas(int(round(CM_CY_CCC[i][0]))))
		ccc_ly.append(commas(int(round(CM_LY_CCC[i][0]))))

	#         value_impact_1= ( ((DV_CM_CY[(DV_CM_CY['Line Item']=='Revenues') & (DV_CM_CY['ABB Division']== i)]['FTM CY'].sum()/ 1000) + (DV_CM_CY[(DV_CM_CY['Line Item']=='Revenues') & (DV_CM_CY['ABB Division']== i)]['FTM LY'].sum()/1000)) / 2 )

	#         value_impact_2= (value_impact_1* 12) / 365

	#         value_impact= commas(int(round(value_impact_2* (CM_CY_CCC[i][0]- CM_LY_CCC[i][0]))))

	#         val_impact.append(value_impact)

	m = month_code
	if time_period == 'QTD':
		m = qtd_month_code

	tab_head_2 = 'DSO' + ' ' + str(year_code) + str(month_code) + ' A'
	tab_head_3 = 'DSO' + ' ' + str(last_year_code) + str(m) + ' A'
	tab_head_4 = 'DIH' + ' ' + str(year_code) + str(month_code) + ' A'
	tab_head_5 = 'DIH' + ' ' + str(last_year_code) + str(m) + ' A'
	tab_head_6 = 'DPO' + ' ' + str(year_code) + str(month_code) + ' A'
	tab_head_7 = 'DPO' + ' ' + str(last_year_code) + str(m) + ' A'
	tab_head_8 = 'CCC' + ' ' + str(year_code) + str(month_code) + ' A'
	tab_head_9 = 'CCC' + ' ' + str(last_year_code) + str(m) + ' A'

	table_dict = {}
	table_dict['type'] = 'table'
	# Table name Should be unique
	table_dict['name'] = 'Table Name'
	# List of table columns in sequence to be showm
	table_dict['headers'] = ['Division', tab_head_2, tab_head_3, tab_head_4, tab_head_5, tab_head_6, tab_head_7,
							 tab_head_8, tab_head_9]
	# List of Object of values
	table_dict['value'] = [
		{'Division': 'Electrification Products', tab_head_2: dso_cy[1], tab_head_3: dso_ly[1], tab_head_4: dih_cy[1],
		 tab_head_5: dih_ly[1], tab_head_6: dpo_cy[1], tab_head_7: dpo_ly[1], tab_head_8: ccc_cy[1],
		 tab_head_9: ccc_ly[1]},
		#         {'Division': 'Electrification Products', tab_head_2: dso_cy[1], tab_head_3: dso_ly[1], tab_head_4: dih_cy[1], tab_head_5: dih_ly[1], tab_head_6: dpo_cy[1], tab_head_7: dpo_ly[1], tab_head_8: ccc_cy[1], tab_head_9: ccc_ly[1], 'MINR Impact': val_impact[1]},
		{'Division': 'Robotics and Motion', tab_head_2: dso_cy[2], tab_head_3: dso_ly[2], tab_head_4: dih_cy[2],
		 tab_head_5: dih_ly[2], tab_head_6: dpo_cy[2], tab_head_7: dpo_ly[2], tab_head_8: ccc_cy[2],
		 tab_head_9: ccc_ly[2]},
		#         {'Division': 'Robotics and Motion', tab_head_2: dso_cy[2], tab_head_3: dso_ly[2], tab_head_4: dih_cy[2], tab_head_5: dih_ly[2], tab_head_6: dpo_cy[2], tab_head_7: dpo_ly[2], tab_head_8: ccc_cy[2], tab_head_9: ccc_ly[2], 'MINR Impact': val_impact[2]},
		{'Division': 'Industrial Automation', tab_head_2: dso_cy[3], tab_head_3: dso_ly[3], tab_head_4: dih_cy[3],
		 tab_head_5: dih_ly[3], tab_head_6: dpo_cy[3], tab_head_7: dpo_ly[3], tab_head_8: ccc_cy[3],
		 tab_head_9: ccc_ly[3]},
		#         {'Division': 'Industrial Automation', tab_head_2: dso_cy[3], tab_head_3: dso_ly[3], tab_head_4: dih_cy[3], tab_head_5: dih_ly[3], tab_head_6: dpo_cy[3], tab_head_7: dpo_ly[3], tab_head_8: ccc_cy[3], tab_head_9: ccc_ly[3], 'MINR Impact': val_impact[3]},
		{'Division': 'Power Grids', tab_head_2: dso_cy[4], tab_head_3: dso_ly[4], tab_head_4: dih_cy[4],
		 tab_head_5: dih_ly[4], tab_head_6: dpo_cy[4], tab_head_7: dpo_ly[4], tab_head_8: ccc_cy[4],
		 tab_head_9: ccc_ly[4]},
		#         {'Division': 'Power Grids', tab_head_2: dso_cy[4], tab_head_3: dso_ly[4], tab_head_4: dih_cy[4], tab_head_5: dih_ly[4], tab_head_6: dpo_cy[4], tab_head_7: dpo_ly[4], tab_head_8: ccc_cy[4], tab_head_9: ccc_ly[4], 'MINR Impact': val_impact[4]},
		{'Division': 'ZC (including ZCPG)', tab_head_2: dso_cy[5], tab_head_3: dso_ly[5], tab_head_4: dih_cy[5],
		 tab_head_5: dih_ly[5], tab_head_6: dpo_cy[5], tab_head_7: dpo_ly[5], tab_head_8: ccc_cy[5],
		 tab_head_9: ccc_ly[5]}
		#         {'Division': 'ZC (including ZCPG)', tab_head_2: dso_cy[5], tab_head_3: dso_ly[5], tab_head_4: dih_cy[5], tab_head_5: dih_ly[5], tab_head_6: dpo_cy[5], tab_head_7: dpo_ly[5], tab_head_8: ccc_cy[5], tab_head_9: ccc_ly[5], 'MINR Impact': val_impact[5]}
	]
	table_dict['value'].append(
		{'Division': 'INABB', tab_head_2: dso_cy[0], tab_head_3: dso_ly[0], tab_head_4: dih_cy[0],
		 tab_head_5: dih_ly[0], tab_head_6: dpo_cy[0], tab_head_7: dpo_ly[0], tab_head_8: ccc_cy[0],
		 tab_head_9: ccc_ly[0]})
	#     table_dict['value'].append({'Division': 'INABB', tab_head_2: dso_cy[0], tab_head_3: dso_ly[0], tab_head_4: dih_cy[0], tab_head_5: dih_ly[0], tab_head_6: dpo_cy[0], tab_head_7: dpo_ly[0], tab_head_8: ccc_cy[0], tab_head_9: ccc_ly[0], 'MINR Impact': val_impact[0]})
	return table_dict


def nwc_table(NWC_COMP_CM_CY, currency, month_code, year_code, qtd_month_code, last_year_code, NWC_COMP_CM_LY,
			  time_period, RU_CM_CY, DV_CM_CY, RU_CM_CY_QTD, DV_CM_CY_QTD, rate):
	# print(NWC_COMP_CM_CY.keys())
	div_codes = ['EP', 'RM', 'IA', 'PG', 'ZC']
	NWC_COMP_CM_CY['Division'] = NWC_COMP_CM_CY['BU'].apply(lambda x: x[:2])
	NWC_COMP_CM_LY['Division'] = NWC_COMP_CM_LY['BU'].apply(lambda x: x[:2])
	# NWC_COMP_CM_LY['Division']= NWC_COMP_CM_LY['BU'].apply(lambda x: x[:2])
	df = NWC_COMP_CM_CY.groupby('Division', as_index=False).sum()
	df_ly = NWC_COMP_CM_LY.groupby('Division', as_index=False).sum()
	other_cols = ['Non-trade \nReceivables', 'Advances\nto Suppliers\n/Contractors',
				  'Prepaid Expenses\n/Accrued Income', 'UN2010\nShort-term\nLoans\nGranted Third\nRel.parties',
				  'UN2110\nShort-term\nReceivable\nFinance Leases\n3rdRel.party', 'Non-trade\nPayables',
				  'Invoices\nto come', 'Accrued Expenses/\nDeferred\nIncome-Current', 'Advances\nFrom Customers']

	trade_rec = []
	inventories = []
	sei = []
	trade_pay = []
	bill = []
	others = []
	nwc = []
	nwc_ly = []
	chart_nwc = []
	chart_nwc_ly = []

	m = month_code
	if time_period == 'QTD':
		m = qtd_month_code
	month_str = str(month_code)
	curr_year_str = str(year_code)
	last_year_str = str(last_year_code)
	m_str = str(m)

	# nwc_col_name_cy= 'Current Period\nNet Working\nCapital (NWC)\n0'+ month_str+ '.'+ str(20)+ curr_year_str
	# nwc_col_name_ly= 'Current Period\nNet Working\nCapital (NWC)\n0'+ m_str+ '.'+ str(20)+ last_year_str

	trade_rec.append(commas(currency_calc((df['Trade \nReceivables'].sum() / 1000), currency, rate)))
	inventories.append(commas(currency_calc((df['Inventories'].sum() / 1000), currency, rate)))
	sei.append(commas(currency_calc((df['Sales in \nExcess of \nInvoicing'].sum() / 1000), currency, rate)))
	trade_pay.append(
		currency_calc(((df['Trade \nPayables'].sum() + df['Invoices\nto come'].sum()) / 1000), currency, rate))
	bill.append(currency_calc((df['Billings in\nExcess of\nSales'].sum() / 1000), currency, rate))
	others.append(currency_calc(((df[other_cols[0]].sum() + df[other_cols[1]].sum() + df[other_cols[2]].sum() + df[
		other_cols[3]].sum() + df[other_cols[4]].sum() - df[other_cols[5]].sum() - df[other_cols[6]].sum() - df[
									  other_cols[7]].sum() - df[other_cols[8]].sum()) / 1000), currency, rate))

	nwc.append(commas(
		currency_calc((RU_CM_CY[RU_CM_CY['Line Item'] == 'Net Working Capital (NWC)']['YTD CY'].values[0] / 1000),
					  currency, rate)))
	if time_period == 'YTD':
		nwc_ly.append(commas(
			currency_calc((RU_CM_CY[RU_CM_CY['Line Item'] == 'Net Working Capital (NWC)']['YTD LY'].values[0] / 1000),
						  currency, rate)))
	else:
		nwc_ly.append(commas(currency_calc(
			(RU_CM_CY_QTD[RU_CM_CY_QTD['Line Item'] == 'Net Working Capital (NWC)']['YTD CY'].values[0] / 1000),
			currency, rate)))

	#     print(df[nwc_col_name_cy])
	for i in div_codes:
		trade_rec.append(
			commas(currency_calc((df[df.Division == i]['Trade \nReceivables'].values[0] / 1000), currency, rate)))
		inventories.append(
			commas(currency_calc((df[df.Division == i]['Inventories'].values[0] / 1000), currency, rate)))
		sei.append(commas(
			currency_calc((df[df.Division == i]['Sales in \nExcess of \nInvoicing'].values[0] / 1000), currency, rate)))
		bill.append(
			currency_calc((df[df.Division == i]['Billings in\nExcess of\nSales'].values[0] / 1000), currency, rate))
		trade_pay.append(currency_calc(((df[df.Division == i]['Trade \nPayables'].values[0] +
										 df[df.Division == i]['Invoices\nto come'].values[0]) / 1000), currency, rate))

		other_val = df[df.Division == i][other_cols[0]].values[0] + df[df.Division == i][other_cols[1]].values[0] + \
					df[df.Division == i][other_cols[2]].values[0] + df[df.Division == i][other_cols[3]].values[0] + \
					df[df.Division == i][other_cols[4]].values[0] - df[df.Division == i][other_cols[5]].values[0] - \
					df[df.Division == i][other_cols[6]].values[0] - df[df.Division == i][other_cols[7]].values[0] - \
					df[df.Division == i][other_cols[8]].values[0]

		others.append(currency_calc((other_val / 1000), currency, rate))

		nwc.append(commas(currency_calc((DV_CM_CY[(DV_CM_CY['ABB Division'] == i) & (
					DV_CM_CY['Line Item'] == 'Net Working Capital (NWC)')]['YTD CY'].values[0] / 1000), currency,
										rate)))
		chart_nwc.append((currency_calc((DV_CM_CY[(DV_CM_CY['ABB Division'] == i) & (
					DV_CM_CY['Line Item'] == 'Net Working Capital (NWC)')]['YTD CY'].values[0] / 1000), currency,
										rate)))

		if time_period == 'YTD':
			nwc_ly.append(commas(currency_calc((DV_CM_CY[(DV_CM_CY['ABB Division'] == i) & (
						DV_CM_CY['Line Item'] == 'Net Working Capital (NWC)')]['YTD LY'].values[0] / 1000), currency,
											   rate)))
			chart_nwc_ly.append(currency_calc((DV_CM_CY[(DV_CM_CY['ABB Division'] == i) & (
						DV_CM_CY['Line Item'] == 'Net Working Capital (NWC)')]['YTD LY'].values[0] / 1000), currency,
											  rate))
		else:
			nwc_ly.append(commas(currency_calc((DV_CM_CY_QTD[(DV_CM_CY_QTD['ABB Division'] == i) & (
						DV_CM_CY_QTD['Line Item'] == 'Net Working Capital (NWC)')]['YTD CY'].values[0] / 1000),
											   currency, rate)))
			chart_nwc_ly.append(currency_calc((DV_CM_CY_QTD[(DV_CM_CY_QTD['ABB Division'] == i) & (
						DV_CM_CY_QTD['Line Item'] == 'Net Working Capital (NWC)')]['YTD CY'].values[0] / 1000),
											  currency, rate))

	# print(bill)
	list_names = [trade_rec, inventories, sei, trade_pay, bill, others, nwc]
	for j in list_names:
		for n, i in enumerate(j):
			if i == 0 or i == '0':
				# print(n)
				j[n] = '-'
				# print(j)
	# print(bill)

	for i in range(0, 6):
		bill[bill.index(bill[i])] = commas(bill[i])
		bill[bill.index(bill[i])] = "<span style='color:red'>" + str(bill[i]) + "</span>"

		trade_pay[trade_pay.index(trade_pay[i])] = commas(trade_pay[i])
		trade_pay[trade_pay.index(trade_pay[i])] = "<span style='color:red'>" + str(trade_pay[i]) + "</span>"

		if others[i] <= 0:
			others[others.index(others[i])] = -others[i]
			others[others.index(others[i])] = commas(others[i])
			others[others.index(others[i])] = "<span style='color:red'>" + str(others[i]) + "</span>"

		else:
			pass

	nwc_header_cy = 'NWC ' + str(year_code) + str(month_code)
	nwc_header_ly = 'NWC ' + str(last_year_code) + str(m)

	table_dict = {}
	table_dict['type'] = 'table'
	# Table name Should be unique
	table_dict['name'] = 'NWC'
	# List of table columns in sequence to be showm
	table_dict['headers'] = ['Division', 'Trade Receivables', 'Inventories', 'Contractual Assets',
							 'Trade payables inc inv to come', 'Billing in Excess of Sales', 'Others', nwc_header_cy,
							 nwc_header_ly]
	# List of Object of values
	table_dict['value'] = [

		{'Division': 'Electrification Products', 'Trade Receivables': trade_rec[1], 'Inventories': inventories[1],
		 'Contractual Assets': sei[1], 'Trade payables inc inv to come': trade_pay[1],
		 'Billing in Excess of Sales': bill[1], 'Others': others[1], nwc_header_cy: nwc[1], nwc_header_ly: nwc_ly[1]},

		{'Division': 'Robotics and Motion', 'Trade Receivables': trade_rec[2], 'Inventories': inventories[2],
		 'Contractual Assets': sei[2], 'Trade payables inc inv to come': trade_pay[2],
		 'Billing in Excess of Sales': bill[2], 'Others': others[2], nwc_header_cy: nwc[2], nwc_header_ly: nwc_ly[2]},

		{'Division': 'Industrial Automation', 'Trade Receivables': trade_rec[3], 'Inventories': inventories[3],
		 'Contractual Assets': sei[3], 'Trade payables inc inv to come': trade_pay[3],
		 'Billing in Excess of Sales': bill[3], 'Others': others[3], nwc_header_cy: nwc[3], nwc_header_ly: nwc_ly[3]},

		{'Division': 'Power Grids', 'Trade Receivables': trade_rec[4], 'Inventories': inventories[4],
		 'Contractual Assets': sei[4], 'Trade payables inc inv to come': trade_pay[4],
		 'Billing in Excess of Sales': bill[4], 'Others': others[4], nwc_header_cy: nwc[4], nwc_header_ly: nwc_ly[4]},

		{'Division': 'ZC (including ZCPG)', 'Trade Receivables': trade_rec[5], 'Inventories': inventories[5],
		 'Contractual Assets': sei[5], 'Trade payables inc inv to come': trade_pay[5],
		 'Billing in Excess of Sales': bill[5], 'Others': others[5], nwc_header_cy: nwc[5], nwc_header_ly: nwc_ly[5]}
	]
	table_dict['value'].append({'Division': 'INABB', 'Trade Receivables': trade_rec[0], 'Inventories': inventories[0],
								'Contractual Assets': sei[0], 'Trade payables inc inv to come': trade_pay[0],
								'Billing in Excess of Sales': bill[0], 'Others': others[0], nwc_header_cy: nwc[0],
								nwc_header_ly: nwc_ly[0]})

	for n, i in enumerate(nwc):
		if i == '0':
			nwc[n] = '-'
	for n, i in enumerate(nwc_ly):
		if i == '0':
			nwc[n] = '-'
	chart_data = [nwc[1:], nwc_ly[1:]]

	print(str(chart_data) + "[context.dataset.data[context.dataIndex]]")
	demo_chart_obj = {}
	demo_chart_obj['type'] = 'chart'
	demo_chart_obj['name'] = 'NWC'
	demo_chart_obj['value'] = {
		"type": 'bar',
		"data": {
			"labels": ['EP', 'RM', 'IA', 'PG', 'ZC'],
			"datasets": [
				{

					"label": nwc_header_cy,
					"data": chart_nwc,
					"borderColor": "#a0bc4d",
					"backgroundColor": "#a0bc4d",
				},
				{
					"label": nwc_header_ly,
					"data": chart_nwc_ly,
					"borderColor": "#12B5A5",
					"backgroundColor": "#12B5A5",
				},
			]
		},
		"options": {
			"responsive": False,
			"maintainAspectRatio": False,
			"animation": {
				"duration": 0
			},
			"plugins": {
				"datalabels": {
					"anchor": "center",
					"align": "center",
					"clamp": True,
					"display": True,
					# "formatter": "value",
					"color": "black",
					"font": {
						"size": 11
					}

				}
				# "extra": {
				# "axis_label_formatter": "value + '%'",
				# "tooltip_label_formatter": "Math.round(tooltipItem.yLabel / data.datasets[tooltipItem.datasetIndex].data.reduce(function(pv, cv) { return pv + cv; }, 0) * 100).toString() + '%'",
				#    "data_label_formatter": "value"+ "%"
				# }
			},
			"legend": {
				"display": True,
				"position": "bottom",
				"labels": {
					"fontSize": 12,
					"boxWidth": 10,
					"boxHeight": 10,
					"fontColor": "black"

				}

			},
			"title": {
				"display": True,
				# "text": "RETENTION as % of RECEIVABLES",
				"text": "Net Working Capital",
				"fontColor": "black",
				"fontSize": 14

			},
			"tooltips": {
				"enabled": False
			},
			"hover": {
				"mode": None
			},
			"scales": {
				"xAxes": [{
					"stacked": True,
					#                     "barThickness": 31,
					"gridLines": {"display": False},
					"ticks": {
						"fontColor": "black"
					},
				}],
				"yAxes": [{
					"stacked": True,
					"ticks": {
						"reverse": False,
						"fontColor": "black",

						# "max": max_val
					},
					"gridLines": {"display": False}
				}]
			}
		},
		"extra": {
			"data_label_formatter": str(chart_data) + "[context.datasetIndex][context.dataIndex]",
			"axis_label_formatter": 'value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");'

		},

		# "extra": {
		# "axis_label_formatter": "value + '%'",
		# "tooltip_label_formatter": "Math.round(tooltipItem.yLabel / data.datasets[tooltipItem.datasetIndex].data.reduce(function(pv, cv) { return pv + cv; }, 0) * 100).toString() + '%'",
		# "data_label_formatter": "value"
		# }
	}

	return table_dict, demo_chart_obj


def overall_nwc_func(RU_CM_CY, NWC_COMP_CM_CY, NWC_COMP_CM_LY, time_period, month_code, qtd_month_code, year_code,
					 last_year_code, currency, RU_CM_CY_QTD, rate):
	if time_period == 'YTD':
		curr_val = RU_CM_CY[RU_CM_CY['Line Item'] == 'Net Working Capital (NWC)']['YTD CY'].values[0]
		prev_val = RU_CM_CY[RU_CM_CY['Line Item'] == 'Net Working Capital (NWC)']['YTD LY'].values[0]
		ret_current_nwc = currency_calc((curr_val) / 1000, currency, rate)
		return currency_calc((curr_val - prev_val) / 1000, currency, rate), ret_current_nwc
	else:
		# print(NWC_COMP_CM_CY.columns)
		#         curr_col= 'Current Period\nNet Working\nCapital (NWC)\n0'+ str(month_code)+ '.20'+ str(year_code)
		#         prev_col= 'Current Period\nNet Working\nCapital (NWC)\n0'+ str(qtd_month_code)+ '.20'+ str(last_year_code)
		#         #curr_nwc= NWC_COMP_CM_CY[NWC_COMP_CM_CY['Current Period\nNet Working\nCapital (NWC)\n0'+ month_code+ '.20'+ year_code]].sum()
		#         #prev_nwc= NWC_COMP_CM_LY[NWC_COMP_CM_LY['Current Period\nNet Working\nCapital (NWC)\n0'+ qtd_month_code+ '.20'+ last_year_code]].sum()
		#         curr_nwc= NWC_COMP_CM_CY[curr_col].sum()/ 1000
		#         prev_nwc= NWC_COMP_CM_LY[prev_col].sum()/ 1000

		curr_nwc = RU_CM_CY[RU_CM_CY['Line Item'] == 'Net Working Capital (NWC)']['YTD CY'].values[0]
		prev_nwc = RU_CM_CY_QTD[RU_CM_CY_QTD['Line Item'] == 'Net Working Capital (NWC)']['YTD CY'].values[0]
		ret_current_nwc = currency_calc((curr_nwc), currency, rate)

		return currency_calc((curr_nwc - prev_nwc), currency, rate), ret_current_nwc


def currency_calc(val, currency, rate):
	if currency == 'MUSD':
		return round((val / rate), 2)
	else:
		return int(round(val / rate))


def map_func(df, div_codes, bu_codes, pg_codes):
	# print(df.shape)
	# df['sas_code']= df['sas_code'].astype(str)
	# start = timeit.default_timer()

	new_column_list = []
	for i in range(0, len(df)):
		# start = timeit.default_timer()
		code = df['sas_code'].iloc[i][4:]
		# print(timeit.default_timer() - start)
		if code in div_codes:
			category = 'DIV'
		elif code in bu_codes:
			category = 'BU'
		elif code in pg_codes:
			category = 'PG'
		else:
			start = timeit.default_timer()
			if ('Z' in code) and len(code) == 2:
				category = 'DIV'
			elif ('Z' in code) and len(code) == 4:
				category = 'BU'
			# lif len(code)==4 and
			#   category = 'PG'
			else:
				category = 'NOT IDENTIFIED'
			# print(timeit.default_timer() - start)
		new_column_list.append(category)

	# start = timeit.default_timer()
	df['Level_Type'] = new_column_list
	# print(timeit.default_timer() - start)
	return df


def clean_cashconversion_sheets(df, div_codes, bu_codes, pg_codes):
	start = timeit.default_timer()
	# df.rename(columns= {'Unnamed: 2':'days', 'Unnamed: 1':'name', 'Profit Center':'sas_code'}, inplace= True)
	df['Level_Type'] = ""
	df['Level_Name'] = ""

	sas_code_list = list(df['sas_code'])
	df['Level_Name'] = [x[4:] for x in sas_code_list]
	# for i in range(0, len(df)):
	#	df['Level_Name'].iloc[i]= df['sas_code'].iloc[i][4:]
	df = map_func(df, div_codes, bu_codes, pg_codes)

	# print(timeit.default_timer() - start)
	return df


# .......................................................................WATERFALL CHART FUNCTIONS.............................................................................
def get_waterfalls(div_codes, lys, cys, dihs, dsos, dpos):
	# start= timeit.default_timer()
	waterfalls_by_div = {c: [lys[i], dsos[i], dihs[i], -1 * dpos[i], cys[i]] for i, c in enumerate(div_codes)}
	# print(timeit.default_timer() - start)
	color_dict = {}
	color_list = []
	color_list = ["#EE7600", "#EE7600"]
	for i in waterfalls_by_div:
		for j in range(1, 4):
			if j < 3:
				if waterfalls_by_div[i][j] > 0:
					color_list.insert(j, "#FF0000")
				else:
					color_list.insert(j, "#008000")
			else:
				if waterfalls_by_div[i][j] > 0:
					color_list.insert(j, "#FF0000")
				else:
					color_list.insert(j, "#008000")
		color_dict.update({i: color_list})
		print(color_list)
		color_list = ["#EE7600", "#EE7600"]
	return waterfalls_by_div, color_dict


def get_stacked_values(waterfall):
	# start= timeit.default_timer()
	filled = []
	transparent = []
	for i, j in enumerate(waterfall):
		if i == 0 or i == 4:
			filled.append(j)
			transparent.append(0)
			base = j
			continue
		total_height = max(base, base + j)
		base = base + j
		filled.append(abs(j))
		transparent.append(total_height - abs(j))
	# print(timeit.default_timer() - start)
	return transparent, filled


def create_waterfall(waterfalls, i, year_code, last_year_code, month_code, qtd_month_code, time_period, color_dict):
	# start= timeit.default_timer()
	transparent, filled = get_stacked_values(waterfalls[i])
	max_val = (round(max(max(transparent), max(filled)) / 10) * 10) + 60

	print(filled)
	# color_list= ["#EE7600", "#12B5A5", "#95A5A6", "#a0bc4d", "#EE7600"]
	# color_list= ["#EE7600", "#EE7600"]
	# for j in range(1, 4):
	#    if j<3:
	#        if filled[j]> 0:
	#            color_list.insert(j, "#FF0000")
	#        else:
	#            color_list.insert(j, "EE7600")
	#    else:
	#        if filled[j]> 0:
	#            color_list.insert(j, "EE7600")
	#        else:
	#            color_list.insert(j, "#FF0000")

	# color_list[color_list[0]]= "#EE7600"
	# color_list[color_list[4]]= "#EE7600"

	if time_period == 'QTD':
		m = qtd_month_code
	else:
		m = month_code
	# (round(max(retention_cm_cy)/10)* 10)+ 20
	# print(color_dict[i])
	demo_chart_obj = {}
	demo_chart_obj['type'] = 'chart'
	demo_chart_obj['name'] = 'CCC'
	demo_chart_obj['value'] = {
		"type": 'bar',

		"data": {
			"labels": [last_year_code + m + " CCC", "DSO", "DIH", "DPO", year_code + month_code + " CCC"],
			"datasets": [
				{"label": "", "stack": "stack0", "data": transparent, "backgroundColor": 'transparent', "datalabels": {
					"clamp": True,
					"anchor": "end",
					"align": "top",
					"display": False}
				 },
				{"label": "A", "stack": "stack0", "data": filled, "backgroundColor": color_dict[i], "datalabels": {
					"clamp": True,
					"anchor": "end",
					"align": "top",
					"display": True,
					"fontSize": 10, "color": "black"

				}
				 },
			]
		},
		"options": {
			"responsive": False,
			"maintainAspectRatio": False,
			"animation": {
				"duration": 0
			},
			"title": {
				"display": True,
				"text": 'CASH CONVERSION CYCLE: ' + i,
				"padding": 22, "position": "top",
				"fontColor": "black"
			},
			"legend": {
				"display": False,
				"labels": {
					"fontColor": "black"

				}
			},
			"tooltips": {
				"enabled": False
			},
			"hover": {
				"mode": None
			},
			# "plugins": {
			#    "datalabels": {
			#        "fontSize": 10,
			#        "fontColor": "black"
			#        "anchor": "end",
			#        "align": "top",
			#        "display": False
			#    }
			# },
			"scales": {
				"xAxes": [
					{"gridLines": {"display": False},
					 "ticks":
						 {
							 "fontColor": "black",

							 # "max" : max_val
						 },
					 }
				]
				,
				# "yAxes": [{"stacked":True},
				#          {"ticks":
				#           {
				#          "reverse": False,
				#          "max" : max(max(transparent), max(filled))
				#           }
				#  }]
				"yAxes": [{
					"stacked": True,
					"ticks":
						{
							"reverse": False,
							"fontColor": "black",

							# "max" : max_val
						},
					"gridLines": {"display": False}
				}]

			}
		}
	}
	# print(timeit.default_timer() - start)
	return demo_chart_obj


# ................................................COMMENT 1 FUNCTIONS: CCC BY DIVISION..............................................................
def bu_ccc_comment(bu_codes, DSO, DIH, DPO):
	# start= timeit.default_timer()
	# ccc= []
	# dso= []
	# dih= []
	# dpo= []
	# pg_list= DIV_BU_MAP[DIV_BU_MAP['Div']== division]['LPG'].unique()
	DSO = DSO[DSO.Level_Type == 'BU']

	DIH = DIH[DIH.Level_Type == 'BU']

	DPO = DPO[DPO.Level_Type == 'BU']
	# print(timeit.default_timer() - start)
	return [DSO, DIH, DPO]


# ........................................

def bu_top_bot(D_file_cy, D_file_ly):
	# start = timeit.default_timer()
	comp_df = pd.merge(D_file_cy, D_file_ly, on='Level_Name', how='outer')
	comp_df.replace('X', 0, inplace=True)
	comp_df['Change'] = comp_df['days_x'] - comp_df['days_y']
	comp_df.fillna(0, inplace=True)
	# top_3_DSO= DSO_comp_df.nlargest(3, 'Change')
	# bottom_3_DSO= DSO_comp_df.nsmallest(3, 'Change')
	# top_3_DSO= list(top_3_DSO['Level_Name'].tolist())
	# bottom_3_DSO= list(bottom_3_DSO['Level_Name'].tolist())
	inc = {}
	dec = {}
	change_list = comp_df['Change'].tolist()
	bu_list = comp_df['Level_Name'].tolist()
	for i, j in zip(change_list, bu_list):
		if i < 0:
			dec.update({j: i})
		elif i > 0:
			inc.update({j: i})
		else:
			pass
		len_top_3 = len(inc)
		len_bot_3 = len(dec)

		if len_top_3 > 3:
			top_3_bu = nlargest(3, inc, key=inc.get)
			len_top_3 = 3
		else:
			top_3_bu = inc.keys()

		if len_bot_3 > 3:
			bottom_3_bu = nsmallest(3, dec, key=dec.get)
			len_bot_3 = 3
		else:
			bottom_3_bu = dec.keys()

		list_obj_top_3 = {}
		list_obj_top_3['type'] = 'list'
		# List of values to be sent only int/string/float values supported
		list_obj_top_3['value'] = top_3_bu

		list_obj_bot_3 = {}
		list_obj_bot_3['type'] = 'list'
		# List of values to be sent only int/string/float values supported
		list_obj_bot_3['value'] = bottom_3_bu
	# print(timeit.default_timer() - start)
	return list_obj_top_3, list_obj_bot_3, len_top_3, len_bot_3


# .....................................................

def div_ccc_comment(div_codes, DSO, DIH, DPO):
	# start= timeit.default_timer()
	ccc = []
	dso = []
	dih = []
	dpo = []

	DSO = DSO[DSO.Level_Type == 'DIV']

	DIH = DIH[DIH.Level_Type == 'DIV']

	DPO = DPO[DPO.Level_Type == 'DIV']

	for i in div_codes:
		dso.append(DSO[DSO.Level_Name == i]['days'].values[0])
		dih.append(DIH[DIH.Level_Name == i]['days'].values[0])
		dpo.append(DPO[DPO.Level_Name == i]['days'].values[0])

	for i in range(0, len(div_codes)):
		ccc.append(dso[i] + dih[i] - dpo[i])

	merged_list = []
	for i in range(0, len(div_codes)):
		merged_list.append([ccc[i], dso[i], dih[i], dpo[i]])

	ccc_dict = dict(zip(div_codes, merged_list))
	# print(timeit.default_timer()- start)
	return ccc_dict


def comment_1(div_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY, bu_codes, pg_codes,
			  year_code, last_year_code, month_code, qtd_month_code, time_period):
	start = timeit.default_timer()

	try:
		cm_cy_overall_dso = round(DSO_CM_CY[DSO_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dso = 0

	try:
		cm_ly_overall_dso = round(DSO_CM_LY[DSO_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dso = 0

	try:
		cm_cy_overall_dih = round(DIH_CM_CY[DIH_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dih = 0

	try:
		cm_ly_overall_dih = round(DIH_CM_LY[DIH_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dih

	try:
		cm_cy_overall_dpo = round(DPO_CM_CY[DPO_CM_CY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_cy_overall_dpo = 0

	try:
		cm_ly_overall_dpo = round([DPO_CM_LY['sas_code'] == 'Overall Result']['days'].item())
	except:
		cm_ly_overall_dpo = 0

	cm_cy_ccc = round((cm_cy_overall_dso + cm_cy_overall_dih - cm_cy_overall_dpo))
	cm_ly_ccc = round((cm_ly_overall_dso + cm_ly_overall_dih - cm_ly_overall_dpo))

	# CM_CY_CCC= div_ccc_comment(div_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY)
	# CM_LY_CCC= div_ccc_comment(div_codes, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY)
	########################################## CCC TABLE
	# dso_cy= []
	# dso_ly= []
	# dih_cy= []
	# dih_ly= []
	# dpo_cy= []
	# dpo_ly= []
	# ccc_cy= []
	# ccc_ly= []
	# val_impact= []

	# dso_cy.append(commas(round(cm_cy_overall_dso)))
	# dso_ly.append(commas(round(cm_ly_overall_dso)))
	# dih_cy.append(commas(round(cm_cy_overall_dih)))
	# dih_ly.append(commas(round(cm_ly_overall_dih)))
	# dpo_cy.append(commas(round(cm_cy_overall_dpo)))
	# dpo_ly.append(commas(round(cm_ly_overall_dpo)))
	# ccc_cy.append(commas(cm_cy_ccc))
	# ccc_ly.append(commas(cm_ly_ccc))
	########### OVERALL CCC WATERFALL...........................................
	overall_ccc = (cm_cy_overall_dso + cm_cy_overall_dih - cm_cy_overall_dpo) - (
				cm_ly_overall_dso + cm_ly_overall_dih - cm_ly_overall_dpo)

	overall_dso_change = round((cm_cy_overall_dso - cm_ly_overall_dso))
	overall_dih_change = round((cm_cy_overall_dih - cm_ly_overall_dih))
	overall_dpo_change = round((cm_cy_overall_dpo - cm_ly_overall_dpo))
	# for returning the current year ccc
	ret_cm_cy_ccc = cm_cy_ccc
	overall_ccc_dict = {'Overall': [cm_ly_ccc, overall_dso_change, overall_dih_change, -overall_dpo_change, cm_cy_ccc]}

	color_dict = {}
	color_list = ["#EE7600", "#EE7600"]
	for i in overall_ccc_dict:
		for j in range(1, 4):
			if j < 3:
				if overall_ccc_dict[i][j] > 0:
					color_list.insert(j, "#FF0000")
					# print(color_list)
				else:
					color_list.insert(j, "#008000")
					# print(color_list)
			else:
				if overall_ccc_dict[i][j] > 0:
					color_list.insert(j, "#FF0000")
					# print(color_list)
				else:
					color_list.insert(j, "#008000")
					# print(color_list)
		color_dict.update({i: color_list})

	overall_ccc_graph = create_waterfall(overall_ccc_dict, 'Overall', year_code, last_year_code, month_code,
										 qtd_month_code, time_period, color_dict)

	# print(overall_ccc)
	# clean_cashconversion_sheets distinguishes based on sas_code whether it's a division, bu or pg
	# start= timeit.default_timer()

	file_list = [DIH_CM_CY, DIH_CM_LY, DSO_CM_CY, DSO_CM_LY, DPO_CM_CY, DPO_CM_LY]
	for i in file_list:
		i = clean_cashconversion_sheets(i, div_codes, bu_codes, pg_codes)
	DIH_CM_CY = file_list[0]
	DIH_CM_LY = file_list[1]
	DSO_CM_CY = file_list[2]
	DSO_CM_LY = file_list[3]
	DPO_CM_CY = file_list[4]
	DPO_CM_LY = file_list[5]
	#     DIH_CM_CY = clean_cashconversion_sheets(DIH_CM_CY, div_codes, bu_codes, pg_codes)
	#     DIH_CM_LY = clean_cashconversion_sheets(DIH_CM_LY, div_codes, bu_codes, pg_codes)
	#     DSO_CM_CY = clean_cashconversion_sheets(DSO_CM_CY, div_codes, bu_codes, pg_codes)
	#     DSO_CM_LY = clean_cashconversion_sheets(DSO_CM_LY, div_codes, bu_codes, pg_codes)
	#     DPO_CM_CY = clean_cashconversion_sheets(DPO_CM_CY, div_codes, bu_codes, pg_codes)
	#     DPO_CM_LY = clean_cashconversion_sheets(DPO_CM_LY, div_codes, bu_codes, pg_codes)

	# print(timeit.default_timer()- start)
	# div_ccc_comment chooses only the divisions, from dso, dih and dpo files and calculates ccc
	CM_CY_CCC = div_ccc_comment(div_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY)
	CM_LY_CCC = div_ccc_comment(div_codes, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY)

	reported_ccc = []
	reported_dso = []
	reported_dih = []
	reported_dpo = []

	list_comment_1 = []

	####################following has bee commented based on new changes...............................
	# for i in div_codes:
	#    reported_ccc = CM_CY_CCC[i][0]- CM_LY_CCC[i][0]
	#    reported_dso = CM_CY_CCC[i][1]- CM_LY_CCC[i][1]
	#    reported_dih = CM_CY_CCC[i][2]- CM_LY_CCC[i][2]
	#    reported_dpo = CM_CY_CCC[i][3]- CM_LY_CCC[i][3]
	#
	#    param_dict= {'DSO': float(reported_dso), 'DIH': float(reported_dih), 'DPO': float(reported_dpo)}
	#    param_list= param_dict.values()
	#    for x in param_list:
	#        max_param= max(param_list, key= abs)
	#
	#    dictt = {
	#        'div_name': i,
	#        'ccc': reported_ccc,
	#        'reported_param_value': max_param,
	#        'reported_param': list(param_dict.keys())[list(param_dict.values()).index(max_param)]
	#    }
	#    list_comment_1.append(dictt)
	##print(list_comment_1)
	########################################

	#####################................................................following calculations are for the ccc graph for each divison.....................................
	cm_cy_ccc = []
	cm_ly_ccc = []

	cm_cy_dso = []
	cm_ly_dso = []
	dso_change = []

	cm_cy_dih = []
	cm_ly_dih = []
	dih_change = []

	cm_cy_dpo = []
	cm_ly_dpo = []
	dpo_change = []

	div_codes = list(div_codes)
	for i in div_codes:
		cm_cy_ccc.append(CM_CY_CCC[i][0])
		cm_ly_ccc.append(CM_LY_CCC[i][0])

		# cm_cy_dso.append(CM_CY_CCC[i][1])
		# cm_ly_dso.append(CM_LY_CCC[i][1])
		dso_change.append(CM_CY_CCC[i][1] - CM_LY_CCC[i][1])

		# cm_cy_dih.append(CM_CY_CCC[i][2])
		# cm_ly_dih.append(CM_LY_CCC[i][2])
		dih_change.append(CM_CY_CCC[i][2] - CM_LY_CCC[i][2])

		# cm_cy_dpo.append(CM_CY_CCC[i][3])
		# cm_ly_dpo.append(CM_LY_CCC[i][3])
		dpo_change.append(CM_CY_CCC[i][3] - CM_LY_CCC[i][3])

	cm_cy_ccc = [round(i) for i in cm_cy_ccc]
	cm_ly_ccc = [round(i) for i in cm_ly_ccc]
	dso_change = [round(i) for i in dso_change]
	dih_change = [round(i) for i in dih_change]
	dpo_change = [round(i) for i in dpo_change]

	# waterfalls, colors = get_waterfalls(div_codes, cm_ly_ccc, cm_cy_ccc, dih_change, dso_change, dpo_change)
	graph_list = []
	# for i in div_codes:
	#    x= create_waterfall(waterfalls, i, year_code, last_year_code, month_code, qtd_month_code, time_period, colors)
	#    graph_list.append(x)
	# transparent, filled = get_stacked_values(waterfalls['EP'])

	# Data and Options Objects must be as per chart.js documentation(<a target='_blank' href="https://www.chartjs.org/docs/latest/">Link</a>)
	# list1 = [0] + cm_ly_ccc
	# list1 = list1[0:-1]
	# list2 =
	# demo_chart_obj = {}
	# demo_chart_obj['type'] = 'chart'
	# demo_chart_obj['name'] = 'CCC'
	# demo_chart_obj['value'] = {
	#    "type": 'bar',
	#    "data": {
	#        "labels": [div_codes[0]],
	#        "datasets":[
	#            {
	#                "label": 'Previous Year',
	#                "data": [cm_ly_ccc[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "#EE7600",
	#                #"stack": "a"
	#            },
	#
	#
	#            {
	#                "label": 'Previous Year',
	#                "data": [cm_ly_ccc[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "a"
	#            }, {
	#                "label": 'Previous Year DIH',
	#                "data": [dih_change[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "#12B5A5",
	#                "stack": "a"
	#            },
	#
	#
	#            {
	#                "label": 'Previous Year',
	#                "data": [cm_ly_ccc[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "b"
	#            }, {
	#                "label": 'Previous Year DIH',
	#                "data": [dih_change[0]+ dso_change[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "b"
	#            },{
	#                "label": 'Previous Year DSO',
	#                "data": [-dso_change[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "#1259DC",
	#                "stack": "b"
	#            },
	#
	#
	#            {
	#                "label": 'Previous Year',
	#                "data": [cm_ly_ccc[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "c"
	#            }, {
	#                "label": 'Previous Year DIH',
	#                "data": [dih_change[0]+ dso_change[0]+ dpo_change[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "c"
	#            },{
	#                "label": 'Previous Year DSO',
	#                "data": [(- dso_change[0])+(- dpo_change[0])],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "Transparent",
	#                "stack": "c"
	#            }, {
	#                "label": 'Previous Year DPO',
	#                "data": [-dpo_change[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "#6256E1",
	#                "stack": "c"
	#            },
	#
	#
	#            {
	#                "label": 'Current Year',
	#                "data": [cm_cy_ccc[0]],
	#                "borderColor": "#EE7600",
	#                "backgroundColor": "#EE7600",
	#                #"stack": "c"
	#            }
	#
	#        ]
	#
	#    },
	#    "options": {
	#        "responsive": True,
	#        "maintainAspectRatio": True,
	#        "animation": {
	#            "duration": 0
	#        },
	#        "plugins": {
	#            "datalabels": {
	#                "anchor": "end",
	#                "align": "top",
	#                "display": False
	#            }
	#        },
	#        "legend":{
	#            "display": False
	#        },
	#        "title": {
	#            "display": True,
	#            "text": "CASH CONVERSION CYCLE"
	#        },
	#        "scales": {
	#            "xAxes": [{
	#                "barThickness": 25
	#            },{
	#                "stacked": True
	#            }],
	#            "yAxes": [{
	#                "ticks": {
	#                    "reverse": False,
	#                    #"max" : max_val
	#                }
	#            },{
	#                "stacked": True
	#            }]
	#        }
	#    }
	# }

	###################### bu level narrative
	CM_CY = bu_ccc_comment(bu_codes, DSO_CM_CY, DIH_CM_CY, DPO_CM_CY)
	CM_LY = bu_ccc_comment(bu_codes, DSO_CM_LY, DIH_CM_LY, DPO_CM_LY)

	# Top 3 and bottom 3 BUs in terms of DSO
	DSO_CM_CY = CM_CY[0]
	DSO_CM_LY = CM_LY[0]

	list_obj_dso_top_3, list_obj_dso_bot_3, len_DSO_top, len_DSO_bot = bu_top_bot(DSO_CM_CY, DSO_CM_LY)
	# list_obj_dso_top_3, list_obj_dso_bot_3, len_DSO_top, len_DSO_bot= bu_top_bot(DSO_CM_CY, DSO_CM_LY)

	DIH_CM_CY = CM_CY[1]
	DIH_CM_LY = CM_LY[1]

	list_obj_dih_top_3, list_obj_dih_bot_3, len_DIH_top, len_DIH_bot = bu_top_bot(DIH_CM_CY, DIH_CM_LY)
	# list_obj_dih_top_3, list_obj_dih_bot_3, len_DIH_top, len_DIH_bot= bu_top_bot(DIH_CM_CY, DIH_CM_LY)

	DPO_CM_CY = CM_CY[2]
	DPO_CM_LY = CM_LY[2]

	list_obj_dpo_top_3, list_obj_dpo_bot_3, len_DPO_top, len_DPO_bot = bu_top_bot(DPO_CM_CY, DPO_CM_LY)
	# list_obj_dpo_top_3, list_obj_dpo_bot_3, len_DPO_top, len_DPO_bot= bu_top_bot(DPO_CM_CY, DPO_CM_LY)

	# DSO_comp_df= pd.merge(DSO_CM_CY, DSO_CM_LY, on= 'Level_Name', how= 'outer')
	# DSO_comp_df.replace('X', 0, inplace= True)
	# DSO_comp_df['Change']= DSO_comp_df['days_x']- DSO_comp_df['days_y']
	# DSO_comp_df.fillna(0, inplace= True)
	# top_3_DSO= DSO_comp_df.nlargest(3, 'Change')
	# bottom_3_DSO= DSO_comp_df.nsmallest(3, 'Change')
	# top_3_DSO= list(top_3_DSO['Level_Name'].tolist())
	# bottom_3_DSO= list(bottom_3_DSO['Level_Name'].tolist())
	# inc= {}
	# dec= {}
	# change_list= DSO_comp_df['Change'].tolist()
	# bu_list= DSO_comp_df['Level_Name'].tolist()
	# for i,j in zip(change_list, bu_list):
	#    if i< 0:
	#        dec.update({j:i})
	#    elif i>0:
	#        inc.update({j:i})
	#    else:
	#        pass
	# len_DSO_top_3= len(inc)
	# len_DSO_bot_3= len(dec)
	#
	# if len_DSO_top_3>3:
	#    top_3_DSO= nlargest(3, inc, key=inc.get)
	#    len_DSO_top_3= 3
	# else:
	#    top_3_DSO= inc.keys()
	#
	# if len_DSO_bot_3>3:
	#    bottom_3_DSO= nsmallest(3, dec, key=dec.get)
	#    len_DSO_bot_3= 3
	# else:
	#    bottom_3_DSO= dec.keys()

	# list_obj_dso_top_3 = {}
	# list_obj_dso_top_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dso_top_3['value'] = top_3_DSO

	# list_obj_dso_bot_3 = {}
	# list_obj_dso_bot_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dso_bot_3['value'] = bottom_3_DSO

	# Top 3 and bottom 3 BUs in terms of DIH
	# DIH_CM_CY= CM_CY[1]
	# DIH_CM_LY= CM_LY[1]
	# DIH_comp_df= pd.merge(DIH_CM_CY, DIH_CM_LY, on= 'Level_Name', how= 'outer')
	# DIH_comp_df.replace('X', 0, inplace= True)
	# DIH_comp_df['Change']= DIH_comp_df['days_x']- DIH_comp_df['days_y']
	# DIH_comp_df.fillna(0, inplace= True)
	# top_3_DIH= DIH_comp_df.nlargest(3, 'Change')
	# bottom_3_DIH= DIH_comp_df.nsmallest(3, 'Change')
	# top_3_DIH= list(top_3_DIH['Level_Name'].tolist())
	# bottom_3_DIH= list(bottom_3_DIH['Level_Name'].tolist())

	# list_obj_dih_top_3 = {}
	# list_obj_dih_top_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dih_top_3['value'] = top_3_DIH

	# list_obj_dih_bot_3 = {}
	# list_obj_dih_bot_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dih_bot_3['value'] = bottom_3_DIH

	# Top 3 and bottom 3 BUs in terms of DPO
	# DPO_CM_CY= CM_CY[2]
	# DPO_CM_LY= CM_LY[2]
	# DPO_comp_df= pd.merge(DPO_CM_CY, DPO_CM_LY, on= 'Level_Name', how= 'outer')
	# DPO_comp_df.replace('X', 0, inplace= True)
	# DPO_comp_df['Change']= DPO_comp_df['days_x']- DPO_comp_df['days_y']
	# DPO_comp_df.fillna(0, inplace= True)
	# top_3_DPO= DPO_comp_df.nlargest(3, 'Change')
	# bottom_3_DPO= DPO_comp_df.nsmallest(3, 'Change')
	# top_3_DPO= list(top_3_DPO['Level_Name'].tolist())
	# bottom_3_DPO= list(bottom_3_DPO['Level_Name'].tolist())

	# list_obj_dpo_top_3 = {}
	# list_obj_dpo_top_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dpo_top_3['value'] = top_3_DPO

	# list_obj_dpo_bot_3 = {}
	# list_obj_dpo_bot_3['type'] = 'list'
	# List of values to be sent only int/string/float values supported
	# list_obj_dpo_bot_3['value'] = bottom_3_DPO

	# loop
	# Must be returned with the same key name as the loop_obj['name']
	loop_obj = {}
	loop_obj['name'] = "loop_object"
	loop_obj['type'] = "dict"
	loop_obj['value'] = [
		{'sequence': 6, 'parameter': 'DSO', 'x': 'decrease', 'y': 'increase', 'Top 3': list_obj_dso_top_3,
		 'Bottom 3': list_obj_dso_bot_3, 'len_top': len_DSO_top, 'len_bot': len_DSO_bot},
		{'sequence': 7, 'parameter': 'DIH', 'x': 'decrease', 'y': 'increase', 'Top 3': list_obj_dih_top_3,
		 'Bottom 3': list_obj_dih_bot_3, 'len_top': len_DIH_top, 'len_bot': len_DIH_bot},
		{'sequence': 8, 'parameter': 'DPO', 'x': 'increase', 'y': 'decrease', 'Top 3': list_obj_dpo_bot_3,
		 'Bottom 3': list_obj_dpo_top_3, 'len_top': len_DPO_bot, 'len_bot': len_DPO_top}
	]

	print('time_for_comment_1:')
	print(timeit.default_timer() - start)
	return [list_comment_1, graph_list, loop_obj, overall_ccc_graph, round(overall_ccc), ret_cm_cy_ccc]
	# return [demo_chart_obj]


# ...............................................................................................................................


# .........................................................COMMENT 2 FUNCTIONS: OVERDUE...................................................

def clean_overdue_sheet(df, div_codes, bu_codes, pg_codes):
	start = timeit.default_timer()
	# df.rename(columns= {'Profit Center': 'sas_code'}, inplace= True)
	df['Level_Type'] = ""
	df['Level_Name'] = ""
	# print(df.shape)
	# for i in range(0, len(df)):
	# df['Level_Name'].iloc[i]= df['sas_code'].iloc[i][4:]
	sas_code_list = list(df['sas_code'])
	df['Level_Name'] = [x[4:] for x in sas_code_list]
	df = map_func(df, div_codes, bu_codes, pg_codes)
	# print(timeit.default_timer() - start)
	return df


def overdue_func(df, div_codes, bu_codes, pg_codes):
	start = timeit.default_timer()
	df = clean_overdue_sheet(df, div_codes, bu_codes, pg_codes)
	# print(OVERDUE_CM_CY)
	df = df[df.Level_Type == 'DIV']
	df = pd.DataFrame(data=df, columns=['Level_Type', 'Level_Name', 'Total Overdue'])
	# print(timeit.default_timer() - start)
	return df
	# print(df)
	# print(len(df))


def comment_2(OVERDUE_CM_CY, OVERDUE_CM_LY, div_codes, bu_codes, pg_codes, currency, rate):
	start = timeit.default_timer()
	overall_overdue = currency_calc(
		((OVERDUE_CM_CY['Total Overdue'].values[0] - OVERDUE_CM_LY['Total Overdue'].values[0]) / 1000), currency, rate)

	OVERDUE_CM_CY = overdue_func(OVERDUE_CM_CY, div_codes, bu_codes, pg_codes)
	OVERDUE_CM_LY = overdue_func(OVERDUE_CM_LY, div_codes, bu_codes, pg_codes)

	comp_df = pd.merge(OVERDUE_CM_CY, OVERDUE_CM_LY, on='Level_Name', how='outer')
	comp_df = comp_df.fillna(0)
	comp_df.replace('', 0, inplace=True)
	comp_df['Overdue_Change'] = comp_df['Total Overdue_x'] - comp_df['Total Overdue_y']

	# print(overall_overdue)
	# top5_df= comp_df.nlargest(5, 'Overdue_Change')

	names = comp_df['Level_Name'].tolist()
	overdues = comp_df['Overdue_Change'].tolist()

	overdues = [currency_calc(i / 1000, currency, rate) for i in overdues]
	list_comment_2 = [names, overdues]
	#     for i in range(0, len(names)):

	#         overdue_dict= {}
	#         overdue_dict= {
	#             'name': names[i],
	#             'overdue': currency_calc(overdues[i]/1000, currency),
	#         }
	#         list_comment_2.append(overdue_dict)

	# print(list_comment_2)
	print('time for comment_2:')
	print(timeit.default_timer() - start)
	return [list_comment_2, overall_overdue]


# ............................................................................................................................................

# .............................................COMMENT 3+4 FUNCTIONS: SALES IN EXCESS OF INVOICES, INVENTORIES...............................................

# def map_nwc(df, div_codes, bu_codes, pg_codes):
#   #df['sas_code']= df['sas_code'].astype(str)
#   start = timeit.default_timer()

#   for i in range(0, len(df)):
#       code = df['BU'].iloc[i]
#       if code in div_codes:
#           category =  'DIV'
#       elif code in bu_codes:
#           category = 'BU'
#       elif code in pg_codes:
#           category = 'PG'
#       else:
#           category = 'NOT IDENTIFIED'

#       df['Level_Type'].iloc[i]= category
#   print('map:')
#   print(timeit.default_timer() - start) 
#   return df

# def clean_nwc_sheet(df, div_codes, bu_codes, pg_codes):
#    start = timeit.default_timer()
#    #df.rename(columns= {'BU': 'sas_code'}, inplace= True)
#    df['Level_Type']= ""
#    df['Level_Name']= ""
#    for i in range(0, len(df)):
#    	df['Level_Name'].iloc[i]= df['BU'].iloc[i]
#    df =  map_nwc(df, div_codes, bu_codes, pg_codes)
#    print('clean_nwc_sheet:')
#    print(timeit.default_timer() - start) 
#    #print(df)
#    return df

# def clean_nwc_sheet(df, div_codes, bu_codes, pg_codes):
#   df['division']= df['BU'][:3]


def sie_func(df, div_codes, bu_codes, pg_codes):
	start = timeit.default_timer()
	# df['Level_Type']= 'BU'  ...........#
	# df['Level_Name']= df['BU'] .........#
	df['division'] = ''
	# for i in range(0, len(df)):
	df['division'] = df['BU'].apply(lambda x: x[:2])
	# df['division'].iloc[i]= df['BU'].iloc[i][:2]
	df['Level_Name'] = df['division']
	# df =  map_nwc(df, div_codes, bu_codes, pg_codes)
	# df= clean_nwc_sheet(df, div_codes, bu_codes, pg_codes)
	# df= df[df.Level_Type== 'BU']#
	df = pd.DataFrame(data=df, columns=['Level_Type', 'Level_Name', 'Sales in \nExcess of \nInvoicing', 'Inventories'])
	# print('sie_func:')
	# print(timeit.default_timer() - start)
	return df


def comment_3_4(NWC_COMP_CM_CY, NWC_COMP_CM_LY, div_codes, bu_codes, pg_codes, currency, rate):
	start = timeit.default_timer()
	NWC_COMP_CM_CY.fillna(0, inplace=True)
	NWC_COMP_CM_CY.replace('', 0, inplace=True)
	NWC_COMP_CM_LY.fillna(0, inplace=True)
	NWC_COMP_CM_LY.replace('', 0, inplace=True)
	NWC_COMP_CM_CY = sie_func(NWC_COMP_CM_CY, div_codes, bu_codes, pg_codes)
	NWC_COMP_CM_LY = sie_func(NWC_COMP_CM_LY, div_codes, bu_codes, pg_codes)

	# comp_df= pd.merge(NWC_COMP_CM_CY, NWC_COMP_CM_LY, on='Level_Name', how= 'outer')
	# comp_df.fillna(0, inplace= True)
	# comp_df.replace('',0, inplace=True)
	nwc_cm_cy = NWC_COMP_CM_CY.groupby('Level_Name', as_index=False)[
		'Sales in \nExcess of \nInvoicing', 'Inventories'].sum()
	nwc_cm_ly = NWC_COMP_CM_LY.groupby('Level_Name', as_index=False)[
		'Sales in \nExcess of \nInvoicing', 'Inventories'].sum()

	comp_df = pd.merge(nwc_cm_cy, nwc_cm_ly, on='Level_Name', how='outer')
	# comp_df= comp_df.groupby('Level_Name', as_index= False)['Sales in \nExcess of \nInvoicing_x', 'Sales in \nExcess of \nInvoicing_y', 'Inventories_x', 'Inventories_y'].sum()
	# comp_df= comp_df.groupby('Level_Name', as_index= False)['Sales in \nExcess of \nInvoicing_x', 'Sales in \nExcess of \nInvoicing_y', 'Inventories_x', 'Inventories_y'].sum()
	# print(comp_df['Level_Name'])
	comp_df['SEI_Change'] = comp_df['Sales in \nExcess of \nInvoicing_x'] - comp_df[
		'Sales in \nExcess of \nInvoicing_y']
	comp_df['Inventory_Change'] = comp_df['Inventories_x'] - comp_df['Inventories_y']
	comp_df.fillna(0, inplace=True)
	comp_df.replace('', 0, inplace=True)

	sei_curr = NWC_COMP_CM_CY['Sales in \nExcess of \nInvoicing'].sum()
	sei_prev = NWC_COMP_CM_LY['Sales in \nExcess of \nInvoicing'].sum()
	overall_sei = currency_calc((sei_curr - sei_prev) / 1000, currency, rate)
	# print(sei_prev)
	# print(comp_df['Sales in \nExcess of \nInvoicing_y'].sum())
	# print(overall_sei)
	# print('--------')
	inv_curr = NWC_COMP_CM_CY['Inventories'].sum()
	inv_prev = NWC_COMP_CM_LY['Inventories'].sum()
	overall_inv = currency_calc((inv_curr - inv_prev) / 1000, currency, rate)
	# print(overall_inv)
	# top5_sei_df= comp_df.nlargest(5, 'SEI_Change')#
	# top5_inv_df= comp_df.nlargest(5, 'Inventory_Change')#

	names_sei = comp_df['Level_Name'].tolist()
	names_inv = comp_df['Level_Name'].tolist()
	seis = comp_df['SEI_Change'].tolist()
	invs = comp_df['Inventory_Change'].tolist()

	seis = [currency_calc(i / 1000, currency, rate) for i in seis]
	invs = [currency_calc(i / 1000, currency, rate) for i in invs]

	list_comment_3 = [names_sei, seis]
	list_comment_4 = [names_inv, invs]
	#     for i in range(0, len(names_sei)):
	#         seis_dict= {}
	#         inv_dict= {}
	#         seis_dict= {
	#             'name': names_sei[i],
	#             'sei': currency_calc(seis[i]/1000, currency)
	#         }
	#         inv_dict= {
	#             'name': names_inv[i],
	#             'inv': currency_calc(invs[i]/1000, currency)
	#         }
	#         list_comment_3.append(seis_dict)
	#         list_comment_4.append(inv_dict)
	# print(list_comment_3)
	# print(list_comment_4)
	print('time for comment_3_4:')
	print(timeit.default_timer() - start)
	return list_comment_3, list_comment_4, overall_sei, overall_inv


# ...................................................................................................................................................

# .................................................................GRAPH: RETENTION..................................................................


def retention_func(ret_df, nwc_df):
	ret_df.fillna(0, inplace=True)
	ret_df.replace('', 0, inplace=True)
	nwc_df.fillna(0, inplace=True)
	nwc_df.replace('', 0, inplace=True)

	nwc_df['DIVISION'] = nwc_df['BU'].apply(lambda x: x[:2])
	ret_div_list = ret_df['DIVISION'].unique()
	nwc_div_list = nwc_df['DIVISION'].unique()
	div_list = list(set(ret_div_list) & set(nwc_div_list))

	ret_df = ret_df[ret_df['DIVISION'].isin(div_list)]
	nwc_df = nwc_df[nwc_df['DIVISION'].isin(div_list)]

	ret_df = ret_df.groupby('DIVISION', as_index=False)['Balance'].sum()
	ret_df.drop(index=ret_df[ret_df.DIVISION == 'Overall Result'].index, inplace=True)

	# nwc_df['DIVISION']= nwc_df['BU'].apply(lambda x: x[:2])
	nwc_df = nwc_df.groupby('DIVISION', as_index=False)['Trade \nReceivables'].sum()

	comp_df = pd.merge(ret_df, nwc_df, on='DIVISION', how='outer')
	# comp_df['retention_value']= comp_df['Balance']/ comp_df['Trade \nReceivables']
	# comp_df['retention_value'].fillna(0, inplace= True)
	# comp_df['retention_value'].replace('', 0, inplace= True)

	div_list = comp_df['DIVISION'].tolist()
	retention_list = comp_df['Balance'].tolist()
	trade_receivables_list = comp_df['Trade \nReceivables'].tolist()
	dum = []
	dum1 = []
	dum2 = []
	for n, i in enumerate(['EP', 'RM', 'IA', 'PG', 'ZC']):
		if i in div_list:
			ind = div_list.index(i)
			# div_list[n] = i
			dum.append(i)
			dum1.append(retention_list[ind])
			dum2.append(trade_receivables_list[ind])
	div_list = dum
	retention_list = dum1
	trade_receivables_list = dum2

	return [div_list, retention_list, trade_receivables_list]
	# print(comp_df)


def retention_graph(NWC_COMP_CM_CY, NWC_COMP_CM_LY, RETENTION_CM_CY, RETENTION_CM_LY, currency, rate):
	cm_cy = retention_func(RETENTION_CM_CY, NWC_COMP_CM_CY)
	# cm_ly= retention_func(RETENTION_CM_LY, NWC_COMP_CM_LY)

	retention_cm_cy = []
	# retention_cm_ly= []
	retention_cm_cy = cm_cy[1]
	# retention_cm_ly= cm_ly[1]
	trade_rec_cm_cy = cm_cy[2]

	div_list_cm_cy = []
	# div_list_cm_ly= []
	div_list_cm_cy = cm_cy[0]
	# div_list_cm_ly= cm_ly[0]
	ret_int = [(currency_calc(i / 1000, currency, rate)) for i in retention_cm_cy]
	trade_int = [(currency_calc(i / 1000, currency, rate)) for i in trade_rec_cm_cy]

	retention_cm_cy = [commas(currency_calc(i / 1000, currency, rate)) for i in retention_cm_cy]
	trade_rec_cm_cy = [commas(currency_calc(i / 1000, currency, rate)) for i in trade_rec_cm_cy]
	table_dict = {}
	table_dict['type'] = 'table'
	# Table name Should be unique
	table_dict['name'] = 'Table Name'
	# List of table columns in sequence to be showm
	table_dict['headers'] = ['Division', 'Retention', 'Trade Receivables']
	# List of Object of values
	table_dict['value'] = [
		{'Division': div_list_cm_cy[0], 'Retention': retention_cm_cy[0], 'Trade Receivables': trade_rec_cm_cy[0]},
		{'Division': div_list_cm_cy[1], 'Retention': retention_cm_cy[1], 'Trade Receivables': trade_rec_cm_cy[1]},
		{'Division': div_list_cm_cy[2], 'Retention': retention_cm_cy[2], 'Trade Receivables': trade_rec_cm_cy[2]},
		{'Division': div_list_cm_cy[3], 'Retention': retention_cm_cy[3], 'Trade Receivables': trade_rec_cm_cy[3]},
		{'Division': div_list_cm_cy[4], 'Retention': retention_cm_cy[4], 'Trade Receivables': trade_rec_cm_cy[4]},
	]

	# retention_difference= []
	# for i in range(0, len(retention_cm_cy)):
	#    retention_difference.append(retention_cm_cy[i]- retention_cm_ly[i])
	# retention_difference= [round(i, 2) for i in retention_difference]
	# retention_cm_cy= [round((round(i, 2)* 100)) for i in retention_cm_cy]
	# retention_cm_cy= [((round(i, 2))) for i in retention_cm_cy]
	# Data and Options Objects must be as per chart.js documentation(<a target='_blank' href="https://www.chartjs.org/docs/latest/">Link</a>)
	# max_val= (round(max(retention_cm_cy)/10)* 10)+ 20
	# retention_cm_cy= [str(i) for i in retention_cm_cy]
	for n, i in enumerate(retention_cm_cy):
		if i == '0':
			retention_cm_cy[n] = '-'
	for n, i in enumerate(trade_rec_cm_cy):
		if i == '0':
			trade_rec_cm_cy[n] = '-'

	chart_data = [retention_cm_cy, trade_rec_cm_cy]
	demo_chart_obj = {}
	demo_chart_obj['type'] = 'chart'
	demo_chart_obj['name'] = 'RETENTION'
	demo_chart_obj['value'] = {
		"type": 'horizontal-bar',
		"data": {
			"labels": div_list_cm_cy,
			"datasets": [
				{
					"label": 'RETENTION',
					"data": ret_int,
					"borderColor": "#e74c3c",
					"backgroundColor": "#e74c3c",
				},
				{
					"label": 'TRADE RECEIVABLES',
					"data": trade_int,
					"borderColor": "#95a5a6",
					"backgroundColor": "#95a5a6",
				},
			]
		},
		"options": {
			"responsive": False,
			"maintainAspectRatio": False,
			"animation": {
				"duration": 0
			},
			"plugins": {
				"datalabels": {
					"anchor": "center",
					"align": "center",
					"clamp": True,
					"rotation": 90,

					"display": True,
					# "formatter": "value",
					"color": "black",
					"font": {
						"size": 11
					}

				}
				# "extra": {
				# "axis_label_formatter": "value + '%'",
				# "tooltip_label_formatter": "Math.round(tooltipItem.yLabel / data.datasets[tooltipItem.datasetIndex].data.reduce(function(pv, cv) { return pv + cv; }, 0) * 100).toString() + '%'",
				#    "data_label_formatter": "value"+ "%"
				# }
			},
			#             "extra": {
			#                     "data_label_formatter":
			#                 },
			"legend": {
				"display": True,
				"position": "bottom",
				"labels": {
					"fontSize": 12,
					"boxWidth": 10,
					"boxHeight": 10,
					"fontColor": "black",

				}

			},
			"title": {
				"display": True,
				# "text": "RETENTION as % of RECEIVABLES",
				"text": "RETENTION by Division",
				"fontColor": "black",
				"fontSize": 14
			},
			"tooltips": {
				"enabled": False
			},
			"hover": {
				"mode": None
			},
			"scales": {
				"xAxes": [{
					"stacked": True,
					# "barThickness": 31,
					"gridLines": {"display": False},
					"ticks": {
						"fontColor": "black"

						# "max": max_val
					},
				}],
				"yAxes": [{
					"stacked": True,
					# "barThickness": 31,
					"ticks": {
						"reverse": False,
						"fontColor": "black"

						# "max": max_val
					},
					"gridLines": {"display": False}
				}]
			}
		},
		"extra": {
			"data_label_formatter": str(chart_data) + "[context.datasetIndex][context.dataIndex]",
			"axis_label_formatter": 'value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");'

		},
		# "extra": {
		# "axis_label_formatter": "value + '%'",
		# "tooltip_label_formatter": "Math.round(tooltipItem.yLabel / data.datasets[tooltipItem.datasetIndex].data.reduce(function(pv, cv) { return pv + cv; }, 0) * 100).toString() + '%'",
		# "data_label_formatter": "value"
		# }
	}
	return demo_chart_obj, table_dict

################################################################################################################################################################
